package by.itacademy.cource.viewandviewgroups;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Экран содержит кастомный контейнер, который добавляет сем в себя элементы с текстом
 * https://developer.android.com/guide/topics/ui/custom-components
 */
public class DynamicLayoutActivity extends AppCompatActivity {

    private EditText viewEditText;
    private Button viewButtonAddItem;
    private Button viewClearItems;
    private DynamicLayout dynamicLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_layout);

        // Ищем в разметке нужные нам элементы экрана
        viewEditText = findViewById(R.id.viewEditText);
        dynamicLayout = findViewById(R.id.viewItemContainer);
        viewClearItems = findViewById(R.id.viewClearItems);
        viewClearItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Очищаем контейнер
                dynamicLayout.removeAllViews();
            }
        });
        viewButtonAddItem = findViewById(R.id.viewButtonAddItem);
        viewButtonAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = viewEditText.getText().toString();
                if (!text.isEmpty()) {
                    // Добавляем элемент в контейнер
                    dynamicLayout.addItem(text);
                }
            }
        });
    }
}
