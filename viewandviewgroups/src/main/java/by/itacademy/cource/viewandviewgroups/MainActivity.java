package by.itacademy.cource.viewandviewgroups;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

/**
 * Экран содержит кнопки, которые должны открывать нужный экран с разметкой.
 *
 * В данном случае View.OnClickListener был подключен к активити, чтобы не генерировать много
 * однотипных лисененров для кнопок
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btnOpenLinearLayout).setOnClickListener(this);
        findViewById(R.id.btnOpenFrameLayout).setOnClickListener(this);
        findViewById(R.id.btnOpenRelativeLayout).setOnClickListener(this);
        findViewById(R.id.btnOpenTableLayout).setOnClickListener(this);
        findViewById(R.id.btnOpenConstraintLayout).setOnClickListener(this);
        findViewById(R.id.btnOpenRecyclerView).setOnClickListener(this);
        findViewById(R.id.btnOpenComplexLayout).setOnClickListener(this);
        findViewById(R.id.btnOpenDynamicLayout).setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Генерируем меню из файла, который лежит в ресурсах в директории Menu
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        // Программное добавление элементов меню
        menu.add(Menu.NONE, R.id.menuItemCustom, Menu.NONE, "Item name");
        return true;
    }

    /**
     * Реагируем если пользователь выбрал какой-то пункт меню. Проверка производится на основе ID
     * нажатого элемента
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuItemOpenActivity:
                startActivity(new Intent(this, ComplexLayoutActivity.class));
                return true;
            case R.id.menuItemCloseApp:
                // останавливаем активити
                finish();
                return true;
            case R.id.subMenu1:
                /*
                 * Показывает тост
                 * https://developer.android.com/guide/topics/ui/notifiers/toasts
                 */
                Toast.makeText(this, "Toast has been shown", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.subMenu2:
                /*
                 * Показываем Snackbar
                 * https://developer.android.com/training/snackbar/showing
                 */
                Snackbar.make(findViewById(R.id.rootLayout), "Snackbar has been shown", Snackbar.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Обрабатываем нажатие кнопок.
     * Получаем ID кнопки и решаем какой экран открыть
     */
    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        switch (viewId) {
            case R.id.btnOpenLinearLayout:
                startActivity(new Intent(this, LinearLayoutActivity.class));
                break;
            case R.id.btnOpenFrameLayout:
                startActivity(new Intent(this, FrameLayoutActivity.class));
                break;
            case R.id.btnOpenRelativeLayout:
                startActivity(new Intent(this, RelativeLayoutActivity.class));
                break;
            case R.id.btnOpenTableLayout:
                startActivity(new Intent(this, TableLayoutActivity.class));
                break;
            case R.id.btnOpenConstraintLayout:
                startActivity(new Intent(this, ConstraintLayoutActivity.class));
                break;
            case R.id.btnOpenRecyclerView:
                startActivity(new Intent(this, RecycleViewActivity.class));
                break;
            case R.id.btnOpenComplexLayout:
                startActivity(new Intent(this, ComplexLayoutActivity.class));
                break;
            case R.id.btnOpenDynamicLayout:
                startActivity(new Intent(this, DynamicLayoutActivity.class));
                break;
        }
    }
}
