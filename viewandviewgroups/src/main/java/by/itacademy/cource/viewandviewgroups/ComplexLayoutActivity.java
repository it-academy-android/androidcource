package by.itacademy.cource.viewandviewgroups;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ComplexLayoutActivity extends AppCompatActivity {

    private EditText viewEditText;
    private CheckBox viewCheckBox;
    private Button viewButtonSave;
    private TextView viewSavedText;
    private RecyclerView viewSavedTextRecyclerList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complex_layout);

        viewEditText = findViewById(R.id.viewEditText);
        viewCheckBox = findViewById(R.id.viewCheckBox);
        viewButtonSave = findViewById(R.id.viewButtonSave);
        viewSavedText = findViewById(R.id.viewSavedText);
        viewSavedTextRecyclerList = findViewById(R.id.viewSavedTextRecyclerList);

        initViews();
    }

    private void initViews() {
        viewSavedTextRecyclerList.setAdapter(new RecyclerAdapter(null));
        viewSavedTextRecyclerList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        viewSavedTextRecyclerList.addItemDecoration(new DividerItemDecoration(this, RecyclerView.VERTICAL));

        viewButtonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String typedText = viewEditText.getText().toString();
                String formattedText = viewCheckBox.isChecked() ? typedText.toUpperCase() : typedText.toLowerCase();
                showSavedText(formattedText);
                updateSavedTextRecyclerList(formattedText);
            }
        });

        setCheckBoxTittle(viewCheckBox.isChecked());
        viewCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setCheckBoxTittle(isChecked);
            }
        });
    }

    private void showSavedText(String formattedText) {
        viewSavedText.setText(formattedText);
    }

    private void updateSavedTextRecyclerList(String textToAdd) {
        RecyclerView.Adapter adapter = viewSavedTextRecyclerList.getAdapter();
        if (adapter instanceof RecyclerAdapter) {
            RecyclerAdapter recyclerAdapter = (RecyclerAdapter)adapter;
            recyclerAdapter.addItem(textToAdd);
            recyclerAdapter.notifyDataSetChanged();
        }
    }

    private void setCheckBoxTittle(boolean isChecked) {
        int stateTextResId = isChecked ? R.string.checkboxON : R.string.checkboxOFF;
        String stateText = getString(stateTextResId);
        String checkBoxTitleText = String.format(getString(R.string.checkboxTitle), stateText);
        viewCheckBox.setText(checkBoxTitleText);
    }

    private class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {

        private List<String> itemList;

        RecyclerAdapter(List<String> itemList) {
            this.itemList = itemList;
        }

        @NonNull
        @Override
        public RecyclerAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_view, parent, false);
            return new RecyclerAdapter.RecyclerViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerAdapter.RecyclerViewHolder holder, int position) {
            String dataToShow = itemList.get(position);
            holder.showData(dataToShow);
        }

        @Override
        public int getItemCount() {
            return itemList != null ? itemList.size() : 0;
        }

        void addItem(String textToAdd){
            ArrayList<String> currentList = new ArrayList<String>();
            if (itemList != null && !itemList.isEmpty()){
                currentList.addAll(itemList);
            }
            currentList.add(textToAdd);
            itemList = currentList;
        }

        private class RecyclerViewHolder extends RecyclerView.ViewHolder {

            private TextView textView;

            RecyclerViewHolder(@NonNull View itemView) {
                super(itemView);
                textView = itemView.findViewById(R.id.textView);
            }

            void showData(final String data) {
                textView.setText(data);
            }
        }
    }
}
