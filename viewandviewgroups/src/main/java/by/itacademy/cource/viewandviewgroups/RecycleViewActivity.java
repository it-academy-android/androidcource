package by.itacademy.cource.viewandviewgroups;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Экран содержит прокручиваемый список RecyclerView, который отображает 100 элементов с текстом,
 * который отображает порядковый номер элемента
 *
 * RecyclerView принимает в себя адаптер и LayoutManager
 *
 * Для большей информации см.
 * https://developer.android.com/guide/topics/ui/layout/recyclerview
 */
public class RecycleViewActivity extends AppCompatActivity {

    private static List<Integer> DATA_LIST;

    static {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        for (int i = 0; i < 100; i++) {
            arrayList.add(i);
        }
        DATA_LIST = arrayList;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycleview);

        RecyclerView recyclerView = findViewById(R.id.viewRecyclerView);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, RecyclerView.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        RecyclerAdapter recyclerAdapter = new RecyclerAdapter(DATA_LIST);
        recyclerView.setAdapter(recyclerAdapter);
    }

    /**
     * Адаптер для нашего списка
     */
    private class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {

        private List<Integer> itemList;

        RecyclerAdapter(List<Integer> itemList) {
            this.itemList = itemList;
        }

        @NonNull
        @Override
        public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_view, parent, false);
            return new RecyclerViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
            Integer dataToShow = itemList.get(position);
            holder.showData(dataToShow);
        }

        @Override
        public int getItemCount() {
            return itemList != null ? itemList.size() : 0;
        }

        /**
         * Контейнер, который хранит сылку на разметку нашего элемента списка
         * Он содержит иконку и текст с порядковым номером элемента
         * Иконка двух видов: для четных номеров и нечетных
         */
        private class RecyclerViewHolder extends RecyclerView.ViewHolder {

            private TextView textView;
            private ImageView imageView;

            RecyclerViewHolder(@NonNull View itemView) {
                super(itemView);
                textView = itemView.findViewById(R.id.textView);
                imageView = itemView.findViewById(R.id.imageView);
            }

            void showData(final Integer data) {
                // Проверяем четность номера и решаем какую иконку подсунусть. Иконки лежат в drawable
                int imageResourceId = data % 2 == 0 ? R.drawable.ic_local_cafe : R.drawable.ic_cloud_circle;
                imageView.setImageResource(imageResourceId);

                textView.setText(data.toString());

                // Обработка нажатия на элемент списка
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String message = String.format(view.getContext().getString(R.string.toastMessage), data);
                        Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }

}
