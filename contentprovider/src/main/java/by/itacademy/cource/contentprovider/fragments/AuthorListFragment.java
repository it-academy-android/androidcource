package by.itacademy.cource.contentprovider.fragments;

import android.Manifest;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import by.itacademy.cource.contentprovider.AuthorsProvider;
import by.itacademy.cource.contentprovider.R;
import by.itacademy.cource.contentprovider.model.DBFullFileInfo;

import static androidx.recyclerview.widget.RecyclerView.VERTICAL;

public class AuthorListFragment extends BaseFragment {

    private static final String READ_CONTACT_PERMISSION = Manifest.permission.READ_CONTACTS;
    private static final int READ_CONTACT_PERMISSION_REQUEST_CODE = 10;

    @BindView(R.id.viewContactList)
    RecyclerView viewContactList;

    @BindView(R.id.viewNoContactsText)
    TextView viewNoContactsText;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showContactList(getAllContacts());
    }

    private List<DBFullFileInfo> getAllContacts() {
        Context context = getContext();
        return context != null ? AuthorsProvider.newInstance(context).getAllData() : Collections.emptyList();
    }

    private void remove(int id) {
        Context context = getContext();
        if (context != null) {
            AuthorsProvider.newInstance(context).removeById(id);
        }
    }

    private void showContactList(@NonNull List<DBFullFileInfo> contactList) {
        viewNoContactsText.setVisibility(contactList.isEmpty() ? View.VISIBLE : View.GONE);
        viewContactList.setVisibility(contactList.isEmpty() ? View.GONE : View.VISIBLE);

        ContactListAdapter adapter = (ContactListAdapter) viewContactList.getAdapter();
        if (adapter == null) {
            viewContactList.setAdapter(adapter = new ContactListAdapter());
            Context context = getContext();
            if (context != null) {
                viewContactList.addItemDecoration(new DividerItemDecoration(context, VERTICAL));
            }
        }
        adapter.setItemList(contactList);
    }

    class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ContactListItemViewHolder> {

        private List<DBFullFileInfo> itemList = null;

        @NonNull
        @Override
        public ContactListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact, parent, false);
            return new ContactListItemViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ContactListItemViewHolder holder, int position) {
            holder.bindData(itemList.get(position));
        }

        @Override
        public int getItemCount() {
            return itemList != null ? itemList.size() : 0;
        }

        void setItemList(List<DBFullFileInfo> itemList) {
            this.itemList = itemList;
            notifyDataSetChanged();
        }

        class ContactListItemViewHolder extends RecyclerView.ViewHolder {

            @BindView(R.id.viewContactNameText)
            TextView viewContactNameText;

            @BindView(R.id.viewContactPhoneNumberText)
            TextView viewContactPhoneNumberText;

            ContactListItemViewHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            private void bindData(final DBFullFileInfo itemContact) {
                viewContactNameText.setText(itemContact.getAuthorName());
                viewContactPhoneNumberText.setText(itemContact.getFilePath());
                itemView.setOnClickListener(view -> {
                    remove(itemContact.getId());
                    showContactList(getAllContacts());
                });
            }
        }
    }
}
