package by.itacademy.cource.contentprovider.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import by.itacademy.cource.contentprovider.ContactsProvider;
import by.itacademy.cource.contentprovider.R;
import by.itacademy.cource.contentprovider.model.Contact;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static androidx.core.content.ContextCompat.checkSelfPermission;
import static androidx.recyclerview.widget.RecyclerView.VERTICAL;

public class ContactListFragment extends BaseFragment {

    private static final String READ_CONTACT_PERMISSION = Manifest.permission.READ_CONTACTS;
    private static final int READ_CONTACT_PERMISSION_REQUEST_CODE = 10;

    @BindView(R.id.viewContactList)
    RecyclerView viewContactList;

    @BindView(R.id.viewNoContactsText)
    TextView viewNoContactsText;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getContactsWithUserPermissions();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == READ_CONTACT_PERMISSION_REQUEST_CODE && grantResults.length > 0 &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            List<Contact> contactList = getAllContacts();
            showContactList(contactList);
        }
    }

    private void getContactsWithUserPermissions() {
        Context context = getContext();
        if (context != null) {
            if (checkSelfPermission(context, READ_CONTACT_PERMISSION) == PERMISSION_GRANTED) {
                List<Contact> contactList = getAllContacts();
                showContactList(contactList);
            } else {
                requestPermissions(new String[]{READ_CONTACT_PERMISSION}, READ_CONTACT_PERMISSION_REQUEST_CODE);
            }
        }
    }

    private List<Contact> getAllContacts() {
        Context context = getContext();
        return context != null ? ContactsProvider.newInstance(context).getAllContacts() : Collections.emptyList();
    }

    private void showContactList(@NonNull List<Contact> contactList) {
        viewNoContactsText.setVisibility(contactList.isEmpty() ? View.VISIBLE : View.GONE);
        viewContactList.setVisibility(contactList.isEmpty() ? View.GONE : View.VISIBLE);

        ContactListAdapter adapter = (ContactListAdapter) viewContactList.getAdapter();
        if (adapter == null) {
            viewContactList.setAdapter(adapter = new ContactListAdapter());
            Context context = getContext();
            if (context != null) {
                viewContactList.addItemDecoration(new DividerItemDecoration(context, VERTICAL));
            }
        }
        adapter.setItemList(contactList);
    }

    class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ContactListItemViewHolder> {

        private List<Contact> itemList = null;

        @NonNull
        @Override
        public ContactListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact, parent, false);
            return new ContactListItemViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ContactListItemViewHolder holder, int position) {
            holder.bindData(itemList.get(position));
        }

        @Override
        public int getItemCount() {
            return itemList != null ? itemList.size() : 0;
        }

        void setItemList(List<Contact> itemList) {
            this.itemList = itemList;
            notifyDataSetChanged();
        }

        class ContactListItemViewHolder extends RecyclerView.ViewHolder {

            @BindView(R.id.viewContactImage)
            ImageView viewContactImage;

            @BindView(R.id.viewContactNameText)
            TextView viewContactNameText;

            @BindView(R.id.viewContactPhoneNumberText)
            TextView viewContactPhoneNumberText;

            ContactListItemViewHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            private void bindData(Contact itemContact) {
                viewContactNameText.setText(itemContact.getName());
                viewContactPhoneNumberText.setText(itemContact.getPhoneNumber());
                Glide.with(itemView)
                        .load(itemContact.getPhotoUri())
                        .placeholder(R.drawable.ic_person)
                        .apply(RequestOptions.circleCropTransform())
                        .into(viewContactImage);
            }
        }
    }
}
