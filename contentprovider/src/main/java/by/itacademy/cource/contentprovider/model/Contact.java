package by.itacademy.cource.contentprovider.model;

import android.net.Uri;

public class Contact {

    private String name;
    private String phoneNumber;
    private Uri photoUri;

    public Contact(String name, String phoneNumber, Uri photoUri) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.photoUri = photoUri;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Uri getPhotoUri() {
        return photoUri;
    }
}
