package by.itacademy.cource.contentprovider;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import by.itacademy.cource.contentprovider.model.Contact;

import static by.itacademy.cource.common.StringUtils.isNullOrEmpty;

public class ContactsProvider {

    public static ContactsProvider newInstance(@NonNull Context context) {
        return new ContactsProvider(context.getContentResolver());
    }

    private ContentResolver contentResolver;

    private ContactsProvider(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;
    }

    public List<Contact> getAllContacts() {
        Uri contactUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

        String[] projection = new String[]{
                ContactsContract.Data._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER
        };

        String orderBy = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME;

        Cursor resultCursor = contentResolver.query(contactUri, projection, null, null, orderBy);
        ArrayList<Contact> contactList = new ArrayList<>();
        if (resultCursor != null) {
            if (resultCursor.moveToFirst()) {
                final int idColumnIndex = resultCursor.getColumnIndex(ContactsContract.Data._ID);
                final int nameColumnIndex = resultCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
                final int numberColumnIndex = resultCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

                do {
                    String contactName = resultCursor.getString(nameColumnIndex);
                    String contactNumber = resultCursor.getString(numberColumnIndex);
                    Uri contactPhotoUri = Uri.withAppendedPath(
                            ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(resultCursor.getString(idColumnIndex))),
                            ContactsContract.Contacts.Photo.CONTENT_DIRECTORY
                    );

                    if (!isNullOrEmpty(contactName) && !isNullOrEmpty(contactNumber)) {
                        Contact contact = new Contact(contactName, contactNumber, contactPhotoUri);
                        contactList.add(contact);
                    }
                } while (resultCursor.moveToNext());
            }
            resultCursor.close();
        }

        return contactList;
    }

}
