package by.itacademy.cource.contentprovider.fragments;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Базовый фрагмент, содержащий общую для всех остальных фрагментв логику, а именно:
 * использование библиотеки Buttrknife для автоматического получения ссылок на наши
 * View из разметки
 *
 * https://jakewharton.github.io/butterknife/
 * https://startandroid.ru/ru/blog/470-butter-knife.html
 */
class BaseFragment extends Fragment {

    private Unbinder viewUnbinder;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewUnbinder = ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (viewUnbinder != null) {
            viewUnbinder.unbind();
        }
    }
}
