package by.itacademy.cource.contentprovider;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import by.itacademy.cource.contentprovider.model.DBFullFileInfo;
import by.itacademy.cource.contentprovider.model.StorageType;

public class AuthorsProvider {

    private static Uri URI_ALL_DATA = Uri.parse("content://by.itacademy.cource.storage/DATA/ALL");
    private static Uri URI_REMOVE = Uri.parse("content://by.itacademy.cource.storage/DATA/#");

    public static AuthorsProvider newInstance(@NonNull Context context) {
        return new AuthorsProvider(context.getContentResolver());
    }

    private ContentResolver contentResolver;

    private AuthorsProvider(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;
    }

    public List<DBFullFileInfo> getAllData() {
        Cursor resultCursor = contentResolver.query(URI_ALL_DATA, null, null, null, null);
        ArrayList<DBFullFileInfo> contactList = new ArrayList<>();
        if (resultCursor != null) {
            if (resultCursor.moveToFirst()) {
                int indexID = resultCursor.getColumnIndex("id");
                int indexName = resultCursor.getColumnIndex("author_name");
                int indexFilePath = resultCursor.getColumnIndex("file_path");
                int indexStorageType = resultCursor.getColumnIndex("storage_type");

                do {
                    DBFullFileInfo fullFileInfo = new DBFullFileInfo(
                            resultCursor.getInt(indexID),
                            resultCursor.getString(indexName),
                            resultCursor.getString(indexFilePath),
                            StorageType.fromDatabase(resultCursor.getString(indexStorageType))
                    );

                    contactList.add(fullFileInfo);
                } while (resultCursor.moveToNext());
            }
            resultCursor.close();
        }

        return contactList;
    }

    public void removeById(int id){
        Uri uri = ContentUris.withAppendedId(URI_REMOVE, id);
        contentResolver.delete(uri, null, null);
    }

}
