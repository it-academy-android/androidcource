package by.itacademy.cource.common;

public class StringUtils {

    public static boolean isNullOrEmpty(String stringText) {
        return stringText == null || stringText.isEmpty();
    }

    private StringUtils() {
    }
}
