package by.itacademy.cource.services;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;

import static by.itacademy.cource.services.DownloadFileIntentService.EXTRA_FILE_URL;

public class DownloadFileActivity extends AppCompatActivity {

    private final String URL_EXAMPLE = "https://www.nikonsupport.eu/europe/Manuals/D7000/D7000_En_05.pdf";

    private ProgressBar progressBar;
    private TextView progressTextView;
    private DownloadFileIntentService.DownloadFileIntentServiceBinder serviceBinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_file);
        progressBar = findViewById(R.id.downloadingProgress);
        progressTextView = findViewById(R.id.progressText);
        ((TextView)findViewById(R.id.urlText)).setText(URL_EXAMPLE);
        findViewById(R.id.downloadFileButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent serviceIntent = new Intent(getApplicationContext(), DownloadFileIntentService.class);
                serviceIntent.putExtra(EXTRA_FILE_URL, URL_EXAMPLE);
                getApplicationContext().startService(serviceIntent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent= new Intent(this, DownloadFileIntentService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(serviceConnection);
    }

    private void setProgressVisibility(final int visibility){
        progressBar.post(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(visibility);
            }
        });
    }

    private final ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            if (service instanceof DownloadFileIntentService.DownloadFileIntentServiceBinder) {
                serviceBinder = (DownloadFileIntentService.DownloadFileIntentServiceBinder) service;
                serviceBinder.addListener(onDownLoadStatusListener);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            if (serviceBinder != null) {
                serviceBinder.removeListener(onDownLoadStatusListener);
            }
        }
    };

    private final DownloadFileIntentService.DownloadFileIntentServiceBinder.OnDownLoadStatusListener onDownLoadStatusListener =
        new DownloadFileIntentService.DownloadFileIntentServiceBinder.OnDownLoadStatusListener() {
            @Override
            public void onDownloadStarted(String url) {
                Toast.makeText(DownloadFileActivity.this, "Downloading started", Toast.LENGTH_SHORT).show();
                setProgressVisibility(View.VISIBLE);
            }

            @Override
            public void onDownloadProgress(final int progress) {
//                progressTextView.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        progressTextView.setText(String.valueOf(progress));
//                    }
//                });
            }

            @Override
            public void onDownloadSuccess(@NonNull final File file) {
                Toast.makeText(DownloadFileActivity.this, "File downloaded", Toast.LENGTH_SHORT).show();
                setProgressVisibility(View.GONE);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent pdfOpenIntent = new Intent(Intent.ACTION_VIEW);
                        pdfOpenIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        pdfOpenIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                        if (pdfOpenIntent.resolveActivity(getPackageManager()) != null) {
                            startActivity(pdfOpenIntent);
                        }
                    }
                });
            }

            @Override
            public void onDownloadError(Exception e) {
                Toast.makeText(DownloadFileActivity.this, "Downloading failed", Toast.LENGTH_SHORT).show();
                setProgressVisibility(View.GONE);
            }
        };
}
