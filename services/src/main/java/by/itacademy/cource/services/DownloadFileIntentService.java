package by.itacademy.cource.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import android.app.IntentService;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * IntentService, который принимает интент с URL файла
 * Сам сервис скачивает файл по этому URL и нотифицирует активити о конечном результате
 *
 * https://developer.android.com/reference/android/app/IntentService
 * https://www.vogella.com/tutorials/AndroidServices/article.html
 * https://habr.com/ru/post/349102/
 *
 *
 * PS Пример - это тольк пример использования IntentService. Он не потокобезопасен!
 */
public class DownloadFileIntentService extends IntentService {

    public static final String EXTRA_FILE_URL = "EXTRA_FILE_URL";
    private static final String LOG_TAG = "IntentServiceLOG";

    private final DownloadFileIntentServiceBinder binder;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public DownloadFileIntentService() {
        super("DownloadFileIntentService");
        binder = new DownloadFileIntentServiceBinder();
    }


    /**
     * Сюда приходит запрос к нашему сервису. Каждый такой запуск сервиса происходит в новом потоке
     *
     * @param intent интент с данными для работы сервиса
     */
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String fileUrl = getFileUrl(intent);

        if (!fileUrl.isEmpty()) {
            try {
                binder.notifyDownloadStateStart(fileUrl);
                downloadFile(fileUrl);
            } catch (MalformedURLException e) {
                e.printStackTrace();
                postMessageToLog("Not able to parse URL");
                binder.notifyDownloadStateError(e);
            }
        } else {
            binder.notifyDownloadStateError(new Exception("URL is empty"));
        }
    }

    /**
     * Извлекаем URL путь из пришедшего интента
     *
     * @param intent интент, который пришел в onHandleIntent
     * @return URL путь из пришедшего интента
     */
    private String getFileUrl(@Nullable Intent intent) {
        return intent != null ? intent.getStringExtra(EXTRA_FILE_URL) : "";
    }

    /**
     * Скачиваем файл по нашему URL
     *
     * 1) Парсим URL и создаем соответствующий объект
     * 2) Открываем соединение к файлу по URL-объекту
     * 3) Извлекаем имя файла из URL-заголовка или пути к файлу
     * 4) Создае файл с нашим именем
     * 5) Создаем стримы для чтения файла из открытого соединения
     * 6) Пишем стрим в файл
     * 7) Не забываем закрыть все стримы после окончания скачивания файла или из-за ошибки !!!!!!!!
     *
     * @param fileUrl URL извлеченный из пришедшего интента
     * @throws MalformedURLException ошибка парсинга URL строки
     */
    private void downloadFile(@NonNull String fileUrl) throws MalformedURLException {
        URL url = new URL(fileUrl);
        InputStream inputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            URLConnection urlConnection = url.openConnection();
            inputStream = urlConnection.getInputStream();
            InputStreamReader reader = new InputStreamReader(inputStream);

            String fileName = getDownloadedFileName(urlConnection);
            File file = getFileToWrite(fileName);

            fileOutputStream = new FileOutputStream(file.getPath());

            int amount = 0;
            int next;
            while ((next = reader.read()) != -1) {
                fileOutputStream.write(next);
                if ((amount + next) - amount > 1024 * 1024) {
                    amount += next;
                    binder.notifyDownloadProgress(amount);
                }
            }

            binder.notifyDownloadStateSuccess(file);
        } catch (IOException e) {
            e.printStackTrace();
            postMessageToLog("Not able to read file from remote or write file into directory");
            binder.notifyDownloadStateError(e);
        } finally {
            closeInputStream(inputStream);
            closeOutputStream(fileOutputStream);
        }
    }

    /**
     * Извлекаем имя файла из URL-заголовка или пути к файлу
     *
     * @param urlConnection соединение (канал) к файлу
     * @return имя файла
     */
    private String getDownloadedFileName(@NonNull URLConnection urlConnection) {
        String urlPath = urlConnection.getURL().getPath();
        String fileName = urlPath.substring(urlPath.lastIndexOf("/") + 1);
        String contentDisposition = urlConnection.getHeaderField("Content-Disposition");
        if (contentDisposition != null && !contentDisposition.isEmpty()) {
            String fileNameHeaderMapKey = "filename=";
            int index = contentDisposition.indexOf(fileNameHeaderMapKey);
            if (index > 0) {
                String headersFileName = contentDisposition.substring(index + fileNameHeaderMapKey.length() + 1, contentDisposition.length() - 1);
                if (!headersFileName.trim().isEmpty()) {
                    fileName = headersFileName;
                }
            }
        }
        return fileName;
    }

    /**
     * Создадим файл на базе полученного имени файла во внешнем хранилище устройства.
     * Если файл с таким именем уже есть в файловой системе, то удалим его
     *
     * @param fileName имя файла
     * @return Объект файла, готовый к записи
     */
    private File getFileToWrite(@NonNull String fileName) {
        File file = new File(getFilesDir(), fileName);
        if (file.exists()) {
            if (file.delete()) {
                postMessageToLog("File with name " + fileName + " can not be removed");
            }
        }
        return file;
    }

    private void closeInputStream(InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                postMessageToLog("Not able to close inputStream");
            }
        }
    }

    private void closeOutputStream(OutputStream outputStream) {
        if (outputStream != null) {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                postMessageToLog("Not able to close outputStream");
            }
        }
    }

    private void postMessageToLog(@NonNull String message) {
        Log.d(LOG_TAG, message);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public static class DownloadFileIntentServiceBinder extends Binder {

        interface OnDownLoadStatusListener {
            void onDownloadStarted(String url);
            void onDownloadProgress(int progress);
            void onDownloadSuccess(@NonNull File file);
            void onDownloadError(Exception e);
        }

        private final ArrayList<OnDownLoadStatusListener> listeners = new ArrayList<>();

        void notifyDownloadStateStart(@NonNull final String url) {
            if (!listeners.isEmpty()) {
                for (OnDownLoadStatusListener listener: listeners) {
                    listener.onDownloadStarted(url);
                }
            }
        }

        void notifyDownloadStateSuccess(@NonNull final File file) {
            if (!listeners.isEmpty()) {
                for (OnDownLoadStatusListener listener: listeners) {
                    listener.onDownloadSuccess(file);
                }
            }
        }

        void notifyDownloadStateError(@NonNull final Exception ex) {
            if (!listeners.isEmpty()) {
                for (OnDownLoadStatusListener listener: listeners) {
                    listener.onDownloadError(ex);
                }
            }
        }

        void notifyDownloadProgress(final int progress) {
            if (!listeners.isEmpty()) {
                for (OnDownLoadStatusListener listener: listeners) {
                    listener.onDownloadProgress(progress);
                }
            }
        }

        public void addListener(@NonNull OnDownLoadStatusListener listener) {
            listeners.add(listener);
        }

        public void removeListener(@NonNull OnDownLoadStatusListener listener) {
            listeners.remove(listener);
        }
    }
}
