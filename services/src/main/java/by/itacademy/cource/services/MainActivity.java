package by.itacademy.cource.services;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button viewButtonStartService = findViewById(R.id.viewButtonStartService);
        final Button viewButtonStopService = findViewById(R.id.viewButtonStopService);
        final Button viewButtonOpenDownload = findViewById(R.id.viewButtonOpenDownload);

        viewButtonStartService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SimpleService.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
                viewButtonStartService.setEnabled(false);
                viewButtonStopService.setEnabled(true);
            }
        });


        viewButtonStopService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService(new Intent(MainActivity.this, SimpleService.class));
                viewButtonStartService.setEnabled(true);
                viewButtonStopService.setEnabled(false);
            }
        });

        viewButtonOpenDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, DownloadFileActivity.class));
            }
        });
    }
}
