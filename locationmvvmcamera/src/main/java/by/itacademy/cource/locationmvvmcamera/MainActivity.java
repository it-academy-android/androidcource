package by.itacademy.cource.locationmvvmcamera;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import by.itacademy.cource.locationmvvmcamera.maps.MapFragment;
import by.itacademy.cource.locationmvvmcamera.photoviewer.PhotoViewerFragment;


public class MainActivity extends AppCompatActivity implements MapFragment.OnMapActionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showMapFragment();
    }

    private void showMapFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentContainer, MapFragment.newInstance(), MapFragment.TAG)
                .commit();
    }

    @Override
    public void onOpenPhoto(long id) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentContainer, PhotoViewerFragment.newInstance(id), PhotoViewerFragment.TAG)
                .setCustomAnimations(R.anim.slide_from_right_to_left, R.anim.slide_from_left_to_right)
                .addToBackStack(null)
                .commit();
    }
}
