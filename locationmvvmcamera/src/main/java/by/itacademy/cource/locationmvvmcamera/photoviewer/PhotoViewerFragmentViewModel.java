package by.itacademy.cource.locationmvvmcamera.photoviewer;

import android.app.Application;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.TaskExecutors;

import java.util.function.Function;

import by.itacademy.cource.locationmvvmcamera.repo.Repository;
import by.itacademy.cource.locationmvvmcamera.repo.database.PhotoEntity;

public class PhotoViewerFragmentViewModel extends AndroidViewModel {

    private Repository repository;

    private MutableLiveData<Bitmap> markerLiveData = new MutableLiveData<>();
    private Function<PhotoEntity, Bitmap> mapper;

    public PhotoViewerFragmentViewModel(@NonNull Application application, Repository repository, Function<PhotoEntity, Bitmap> mapper) {
        super(application);
        this.repository = repository;
        this.mapper = mapper;
    }

    LiveData<Bitmap> getMarkerLiveData() {
        return markerLiveData;
    }

    void fetchPhoto(long id) {
        repository.getPhoto(id)
                .thenApplyAsync(mapper)
                .thenAcceptAsync(
                        markerOptions -> markerLiveData.setValue(markerOptions),
                        TaskExecutors.MAIN_THREAD
                );
    }
}
