package by.itacademy.cource.locationmvvmcamera.maps;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.TaskExecutors;

import java.io.File;
import java.util.List;
import java.util.function.Function;

import by.itacademy.cource.locationmvvmcamera.repo.Repository;
import by.itacademy.cource.locationmvvmcamera.repo.database.PhotoEntity;

public class MapFragmentViewModel extends AndroidViewModel {

    private Repository repository;

    private MutableLiveData<List<MarkerState>> markerLiveData = new MutableLiveData<>();
    private Function<List<PhotoEntity>, List<MarkerState>> mapper;

    public MapFragmentViewModel(@NonNull Application application, Repository repository, Function<List<PhotoEntity>, List<MarkerState>> mapper) {
        super(application);
        this.repository = repository;
        this.mapper = mapper;
    }

    LiveData<List<MarkerState>> getMarkerLiveData() {
        return markerLiveData;
    }

    void fetchPhotoMarkers() {
        repository.getPhotoList()
                .thenApplyAsync(mapper)
                .thenAcceptAsync(
                        markerStates -> markerLiveData.setValue(markerStates),
                        TaskExecutors.MAIN_THREAD
                );
    }

    void savePhoto(File photoFile, LatLng position) {
        repository.savePhoto(position, photoFile).thenAccept(aVoid -> fetchPhotoMarkers());
    }
}
