package by.itacademy.cource.locationmvvmcamera.maps;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;
import java.util.stream.Collectors;

import by.itacademy.cource.locationmvvmcamera.repo.database.PhotoEntity;
import by.itacademy.cource.locationmvvmcamera.utils.ImageUtils;

public class PhotoEntityToMarkerOptionsMapper implements Function<List<PhotoEntity>, List<MarkerState>> {

    @Override
    public List<MarkerState> apply(List<PhotoEntity> data) {
        List<MarkerState> mapMarkerList = Collections.emptyList();
        if (data != null && !data.isEmpty()) {
            mapMarkerList = data.stream()
                    .map(this::getMarker)
                    .collect(Collectors.toList());
        }
        return mapMarkerList;
    }

    private MarkerState getMarker(PhotoEntity markerViewModelState) {
        DateFormat dateFormat = new SimpleDateFormat("dd.mm.yyyy", Locale.getDefault());
        MarkerOptions markerOptions = new MarkerOptions().position(markerViewModelState.getCoordintes())
                .title(dateFormat.format(markerViewModelState.getDate()))
                .draggable(false)
                .icon(getBitmapIcon(markerViewModelState.getFilePath()));
        return new MarkerState(markerViewModelState.getId(), markerOptions);
    }

    private BitmapDescriptor getBitmapIcon(File photoFile) {
        Bitmap imageBitmap = ImageUtils.decodeBitmapFromFile(photoFile);
        Bitmap scaled = Bitmap.createScaledBitmap(imageBitmap, 100, 100, true);
        return BitmapDescriptorFactory.fromBitmap(scaled);
    }
}
