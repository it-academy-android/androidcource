package by.itacademy.cource.locationmvvmcamera.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class OkCancelDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {

    public static final String TAG = OkCancelDialogFragment.class.getName();
    private static final String EXTRA_TITLE_KEY = "EXTRA_TITLE_KEY";
    private static final String EXTRA_TEXT_KEY = "EXTRA_TEXT_KEY";

    public interface OnDialogOkActionListener {
        void onOkClick();
    }

    public interface OnDialogCancelActionListener {
        void onCancelClick();
    }

    public static OkCancelDialogFragment newInstance(String title, String text) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_TITLE_KEY, title);
        bundle.putString(EXTRA_TEXT_KEY, text);

        OkCancelDialogFragment fragment = new OkCancelDialogFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    private OnDialogOkActionListener onDialogOkActionListener;
    private OnDialogCancelActionListener onDialogCancelActionListener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof OnDialogOkActionListener) {
            onDialogOkActionListener = (OnDialogOkActionListener) context;
        }

        if (context instanceof OnDialogCancelActionListener) {
            onDialogCancelActionListener = (OnDialogCancelActionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onDialogOkActionListener = null;
        onDialogCancelActionListener = null;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new AlertDialog.Builder(getContext())
            .setTitle(getTitleFromArgs())
            .setMessage(getTextFromArgs())
            .setPositiveButton(android.R.string.ok, this)
            .setNegativeButton(android.R.string.cancel, this)
            .create();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int which) {
        if (which == Dialog.BUTTON_POSITIVE) {
            onOkCalled();
        } else {
            onDismissCalled();
        }
    }

    private void onOkCalled() {
        if (onDialogOkActionListener != null) {
            onDialogOkActionListener.onOkClick();
        }
    }

    private void onDismissCalled() {
        if (onDialogCancelActionListener != null) {
            onDialogCancelActionListener.onCancelClick();
        }
    }

    private String getTitleFromArgs() {
        Bundle bundle = getArguments();
        return bundle != null ? bundle.getString(EXTRA_TITLE_KEY) : "";
    }

    private String getTextFromArgs() {
        Bundle bundle = getArguments();
        return bundle != null ? bundle.getString(EXTRA_TEXT_KEY) : "";
    }
}
