package by.itacademy.cource.locationmvvmcamera.utils;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import by.itacademy.cource.locationmvvmcamera.maps.MapFragmentViewModel;
import by.itacademy.cource.locationmvvmcamera.maps.PhotoEntityToMarkerOptionsMapper;
import by.itacademy.cource.locationmvvmcamera.photoviewer.PhotoEntityToBitmapMapper;
import by.itacademy.cource.locationmvvmcamera.photoviewer.PhotoViewerFragmentViewModel;
import by.itacademy.cource.locationmvvmcamera.repo.RepositoryImpl;

public class ViewModelFactory implements ViewModelProvider.Factory {

    private Application application;

    public ViewModelFactory(Application application) {
        this.application = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(MapFragmentViewModel.class)) {
            return (T) new MapFragmentViewModel(application, new RepositoryImpl(application),
                    new PhotoEntityToMarkerOptionsMapper());
        } else if (modelClass.isAssignableFrom(PhotoViewerFragmentViewModel.class)) {
            return (T) new PhotoViewerFragmentViewModel(application, new RepositoryImpl(application),
                    new PhotoEntityToBitmapMapper());
        }
        return null;
    }
}
