package by.itacademy.cource.locationmvvmcamera.repo;

import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import by.itacademy.cource.locationmvvmcamera.repo.database.PhotoEntity;

public interface Repository {
    CompletableFuture<List<PhotoEntity>> getPhotoList();
    CompletableFuture<PhotoEntity> getPhoto(long id);
    CompletableFuture<Void> savePhoto(LatLng position, File photoFile);
}
