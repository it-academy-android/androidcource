package by.itacademy.cource.locationmvvmcamera.photoviewer;

import android.graphics.Bitmap;

import java.util.function.Function;

import by.itacademy.cource.locationmvvmcamera.repo.database.PhotoEntity;
import by.itacademy.cource.locationmvvmcamera.utils.ImageUtils;

public class PhotoEntityToBitmapMapper implements Function<PhotoEntity, Bitmap> {

    @Override
    public Bitmap apply(PhotoEntity photoEntity) {
        return ImageUtils.decodeBitmapFromFile(photoEntity.getFilePath());
    }
}
