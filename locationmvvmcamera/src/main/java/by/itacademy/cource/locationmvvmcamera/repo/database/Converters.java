package by.itacademy.cource.locationmvvmcamera.repo.database;

import androidx.annotation.Nullable;
import androidx.room.TypeConverter;

import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.util.Date;

public class Converters {

    @TypeConverter
    @Nullable
    public static Date fromTimestamp(@Nullable Long value) {
        return value != null ? new Date(value) : null;
    }

    @TypeConverter
    @Nullable
    public static Long dateToTimestamp(@Nullable Date date) {
        return date != null ? date.getTime() : null;
    }

    @TypeConverter
    @Nullable
    public static String latLongToString(@Nullable LatLng latLng) {
        return latLng != null ? latLng.latitude + "," + latLng.longitude : null;
    }

    @TypeConverter
    @Nullable
    public static LatLng stringToLatLong(@Nullable String stringValue) {
        if (stringValue != null && !stringValue.isEmpty()) {
            String[] valueArray = stringValue.split(",");
            if (valueArray.length > 1) {
                return new LatLng(Double.valueOf(valueArray[0]), Double.valueOf(valueArray[1]));
            }
        }
        return null;
    }

    @TypeConverter
    @Nullable
    public static String fileToString(@Nullable File file) {
        return file != null ? file.getAbsolutePath() : null;
    }

    @TypeConverter
    @Nullable
    public static File stringToFile(@Nullable String filePath) {
        return filePath != null ? new File(filePath) : null;
    }
}
