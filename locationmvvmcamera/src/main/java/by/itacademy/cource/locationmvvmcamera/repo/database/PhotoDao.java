package by.itacademy.cource.locationmvvmcamera.repo.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface PhotoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(PhotoEntity word);

    @Query("DELETE FROM photo WHERE id == :id")
    int delete(long id);

    @Query("SELECT * from photo ORDER BY date")
    List<PhotoEntity> getAll();

    @Query("SELECT * from photo WHERE id == :id ORDER BY date")
    PhotoEntity getPhotoEntity(long id);

}
