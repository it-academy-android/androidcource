package by.itacademy.cource.locationmvvmcamera.maps;

import com.google.android.gms.maps.model.MarkerOptions;

class MarkerState {
    private long photoId;
    private MarkerOptions markerOptions;

    MarkerState(long photoId, MarkerOptions markerOptions) {
        this.photoId = photoId;
        this.markerOptions = markerOptions;
    }

    long getPhotoId() {
        return photoId;
    }

    MarkerOptions getMarkerOptions() {
        return markerOptions;
    }
}
