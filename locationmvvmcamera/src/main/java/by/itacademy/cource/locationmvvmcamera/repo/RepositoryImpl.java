package by.itacademy.cource.locationmvvmcamera.repo;

import android.app.Application;

import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import by.itacademy.cource.locationmvvmcamera.repo.database.PhotoDao;
import by.itacademy.cource.locationmvvmcamera.repo.database.PhotoDatabase;
import by.itacademy.cource.locationmvvmcamera.repo.database.PhotoEntity;

public class RepositoryImpl implements Repository {

    private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() - 1);

    private PhotoDao photoDao;

    public RepositoryImpl(Application application) {
        photoDao = PhotoDatabase.getDatabase(application).photoDao();
    }

    @Override
    public CompletableFuture<List<PhotoEntity>> getPhotoList() {
        return CompletableFuture.supplyAsync(() -> photoDao.getAll(), EXECUTOR)
            .exceptionally(throwable -> Collections.emptyList());
    }

    @Override
    public CompletableFuture<PhotoEntity> getPhoto(final long id) {
        return CompletableFuture.supplyAsync(() -> photoDao.getPhotoEntity(id), EXECUTOR);
    }

    @Override
    public CompletableFuture<Void> savePhoto(LatLng position, File photoFile) {
        final PhotoEntity entity = new PhotoEntity();
        entity.setCoordintes(position);
        entity.setDate(new Date());
        entity.setFilePath(photoFile);

        return CompletableFuture.supplyAsync(() -> {
            photoDao.insert(entity);
            return null;
        }, EXECUTOR);
    }
}
