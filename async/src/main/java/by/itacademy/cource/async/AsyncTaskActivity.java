package by.itacademy.cource.async;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.lang.ref.WeakReference;

/**
 * Активити, которая работает через AsyncTask
 * https://developer.android.com/reference/android/os/AsyncTask
 */
public class AsyncTaskActivity extends BaseActivity {

    private DownloadImageAsyncTask downloadImageAsyncTask;

    @Override
    protected void onStop() {
        super.onStop();
        if (downloadImageAsyncTask != null) {
            downloadImageAsyncTask.cancel(true);
            downloadImageAsyncTask = null;
        }
    }

    @Override
    protected void getImage() {
        downloadImageAsyncTask = new DownloadImageAsyncTask(this);
        downloadImageAsyncTask.execute(BuildConfig.IMG_URL);
    }

    /**
     * AsyncTask, который создает новый поток, скачивает картинку и отдает ее в главный поток
     * Делаем класс статическим чтобы разорвать неявную ссылку на активити
     * <p>
     * WeakReference хранит ссылку на активити, чтобы ее обновить когда это нужно
     * https://habr.com/ru/post/169883/
     */
    private static final class DownloadImageAsyncTask extends AsyncTask<String, Void, Bitmap> {

        private WeakReference<BaseActivity> baseActivityWeakReference;

        DownloadImageAsyncTask(BaseActivity baseActivity) {
            this.baseActivityWeakReference = new WeakReference<>(baseActivity);
        }

        /**
         * Перед началом основной работы можно сделать подготовку в главном потоке
         * В данном случае мы показывает прогресс
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            changeProgressVisibility(View.VISIBLE);
        }

        /**
         * Выполняем основную работу в другом потоке
         */
        @Override
        @Nullable
        protected Bitmap doInBackground(String... strings) {
            try {
                String url = strings[0];
                ImageRequest imageRequest = new ImageRequest();
                return imageRequest.getImageFromUrl(url);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        /**
         * Выполняем работу после скачивания файла в главном потоке, так как UI мы можем обновить только в главном потоке
         */
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            changeProgressVisibility(View.GONE);
            setImage(bitmap);
        }

        private void changeProgressVisibility(int visibility) {
            BaseActivity activity = baseActivityWeakReference.get();
            if (activity != null) {
                activity.progressBar.setVisibility(visibility);
            }
        }

        private void setImage(@Nullable Bitmap bitmap) {
            BaseActivity activity = baseActivityWeakReference.get();
            if (activity != null) {
                if (bitmap != null) {
                    activity.imageView.setImageBitmap(bitmap);
                } else {
                    showError();
                }

            }
        }

        private void showError() {
            BaseActivity activity = baseActivityWeakReference.get();
            if (activity != null) {
                Toast.makeText(activity, "The image was not received", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
