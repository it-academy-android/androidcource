package by.itacademy.cource.async;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.AsyncTaskLoader;
import androidx.loader.content.Loader;

import java.io.IOException;

public class LoaderActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<Bitmap> {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initLoader();
    }

    @Override
    protected void getImage() {
        progressBar.setVisibility(View.VISIBLE);
        startLoader();
    }

    @NonNull
    @Override
    public Loader<Bitmap> onCreateLoader(int id, @Nullable Bundle args) {
        return new ImageLoader(this, args);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Bitmap> loader, Bitmap data) {
        if (data != null) {
            imageView.setImageBitmap(data);
        } else {
            Toast.makeText(this, "The image was not received", Toast.LENGTH_SHORT).show();
        }
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Bitmap> loader) {

    }

    private void initLoader() {
        Bundle bundle = new Bundle();
        bundle.putString(ImageLoader.KEY_URL, BuildConfig.IMG_URL);
        LoaderManager.getInstance(this).initLoader(ImageLoader.LOADER_ID, bundle, this);
    }

    private void startLoader() {
        Loader loader = LoaderManager.getInstance(this).getLoader(ImageLoader.LOADER_ID);
        if (loader != null) {
            loader.forceLoad();
        }
    }

    private static class ImageLoader extends AsyncTaskLoader<Bitmap> {

        static final String KEY_URL = "KEY_URL";
        static final int LOADER_ID = 10000;

        private final String imageUrl;

        ImageLoader(@NonNull Context context, @Nullable Bundle args) {
            super(context);
            imageUrl = args != null ? args.getString(KEY_URL) : "";
        }

        @Nullable
        @Override
        public Bitmap loadInBackground() {
            ImageRequest imageRequest = new ImageRequest();
            try {
                return imageRequest.getImageFromUrl(imageUrl);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
