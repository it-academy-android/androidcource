package by.itacademy.cource.async;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.io.InputStream;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Класс для выполнения сетевых запросов по получению картинки
 */
class ImageRequest {

    private OkHttpClient client = new OkHttpClient();

    /**
     * Делаем синхронный запрос для получения картинки
     */
    Bitmap getImageFromUrl(@NonNull String url) throws IOException {

        // Формируем запрос с картинке
        Request request = new Request.Builder().url(url).build();

        // Шлем запрос на получение каритнки
        // И если он успешный, то получаем картинку иначе прокидываем ошибку
        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Error during request " + request);

        // Получаем тело запроса для подключения к файлу
        ResponseBody responseBody = response.body();
        if (responseBody == null) throw new IOException("Error during getting response bode ");

        // Открываем поток, чтобы скачать файл
        InputStream inputStream = responseBody.byteStream();

        // Скачиваем картинку и преобразовываем ее в  Bitmap
        // http://developer.alexanderklimov.ru/android/catshop/bitmap.php
        return BitmapFactory.decodeStream(inputStream);
    }

}
