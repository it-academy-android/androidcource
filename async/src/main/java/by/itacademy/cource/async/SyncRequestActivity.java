package by.itacademy.cource.async;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * Пример того, что приложение будет падать, если работать с сетью из главного потока.
 * Ожидаемая ошибка NetworkOnMainThreadException
 */
public class SyncRequestActivity extends BaseActivity {

    /**
     * Метод будет генерировать ошибку NetworkOnMainThreadException
     * так как запрос идет с главного потока
     */
    @Override
    protected void getImage() {
        progressBar.setVisibility(View.VISIBLE);
        try {
            ImageRequest imageRequest = new ImageRequest();
            Bitmap image = imageRequest.getImageFromUrl(BuildConfig.IMG_URL);
            imageView.setImageBitmap(image);
        } catch (IOException e) {
            e.printStackTrace();
        }
        progressBar.setVisibility(View.VISIBLE);
    }
}
