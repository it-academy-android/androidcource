package by.itacademy.cource.async;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * Активити, которая будет стартовать новы поток как Thread и возвращать в главный поток сообщения
 * через Handler
 */
public class HandlerActivity extends BaseActivity {

    private static final int STATUS_DOWNLOAD_START = 1; // загрузка началась
    private static final int STATUS_DOWNLOADED = 2; // файл загружен
    private static final int STATUS_DOWNLOAD_END = 3; // загрузка закончена

    private Handler handler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new MessageHandler(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (downloadThread != null) {
            downloadThread.interrupt();
        }
    }

    /**
     * Поток Thread, через который скачивается картинка
     */
    private Thread downloadThread;

    @Override
    protected void getImage() {
        downloadThread = new Thread(new Runnable() {
            @Override
            public void run() {
                ImageRequest imageRequest = new ImageRequest();
                try {
                    handler.sendEmptyMessage(STATUS_DOWNLOAD_START);
                    Bitmap image = imageRequest.getImageFromUrl(BuildConfig.IMG_URL);
                    Message msg = handler.obtainMessage(STATUS_DOWNLOADED, image);
                    handler.sendMessage(msg);
                } catch (IOException e) {
                    e.printStackTrace();
                    handler.sendEmptyMessage(STATUS_DOWNLOAD_END);
                }
            }
        });
        downloadThread.start();
    }


    private static class MessageHandler extends Handler {

        private WeakReference<BaseActivity> baseActivityWeakReference;

        MessageHandler(BaseActivity baseActivity) {
            this.baseActivityWeakReference = new WeakReference<>(baseActivity);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case STATUS_DOWNLOAD_START:
                    changeProgressVisibility(View.VISIBLE);
                    break;
                case STATUS_DOWNLOAD_END:
                    changeProgressVisibility(View.GONE);
                    showError();
                    break;
                case STATUS_DOWNLOADED:
                    changeProgressVisibility(View.GONE);
                    Object msgObject = msg.obj;
                    if (msgObject != null && msgObject instanceof Bitmap) {
                        setImage((Bitmap) msgObject);
                    }
                    break;
            }
        }

        private void changeProgressVisibility(int visibility) {
            BaseActivity activity = baseActivityWeakReference.get();
            if (activity != null) {
                activity.progressBar.setVisibility(visibility);
            }
        }

        private void setImage(@Nullable Bitmap bitmap) {
            BaseActivity activity = baseActivityWeakReference.get();
            if (activity != null) {
                if (bitmap != null) {
                    activity.imageView.setImageBitmap(bitmap);
                } else {
                    showError();
                }
            }
        }

        private void showError() {
            BaseActivity activity = baseActivityWeakReference.get();
            if (activity != null) {
                Toast.makeText(activity, "The image was not received", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
