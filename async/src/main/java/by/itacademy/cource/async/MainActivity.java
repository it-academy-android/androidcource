package by.itacademy.cource.async;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Activity с кнопками для открытия других активити,
 * которые содержат примеры работы с потоками
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.btnOpenSyncTaskActivity).setOnClickListener(this);
        findViewById(R.id.btnOpenAsyncTaskActivity).setOnClickListener(this);
        findViewById(R.id.btnOpenHandlerTaskActivity).setOnClickListener(this);
        findViewById(R.id.btnOpenLoaderTaskActivity).setOnClickListener(this);
    }

    /**
     * Реагируем на нажатие кнопок и открываем соотвествующую активити
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnOpenAsyncTaskActivity:
                startActivity(new Intent(this, AsyncTaskActivity.class));
                break;
            case R.id.btnOpenHandlerTaskActivity:
                startActivity(new Intent(this, HandlerActivity.class));
                break;
            case R.id.btnOpenSyncTaskActivity:
                startActivity(new Intent(this, SyncRequestActivity.class));
                break;
            case R.id.btnOpenLoaderTaskActivity:
                startActivity(new Intent(this, LoaderActivity.class));
                break;
            default:
                break;
        }
    }
}
