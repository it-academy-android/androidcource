package by.itacademy.cource.storage.database.openhelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

/**
 * Класс реализующий SQLiteOpenHelper первоначальной работы с БД:
 * создание базы, ее обновление и т.д.
 *
 * https://developer.android.com/training/data-storage/sqlite#java
 *
 * Класс реализован как классический Singleton что не создавать каждый раз объект базы данных.
 * Способов его реализации уйма: https://habr.com/ru/post/129494/
 */
public class DBHelper extends SQLiteOpenHelper {

    private static DBHelper instance;

    private static final String DATABASE_NAME = "user_saved_files.db"; // название файла БД
    private static final int DATABASE_VERSION = 1; // номер версии БД. Увеличивается при изменении схемы

    public static synchronized DBHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DBHelper(context);
        }
        return instance;
    }

    private UserFilesDAO userFilesDAO;

    private DBHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Вызывается только при создании БД
     * Здесь происходит предварительная инициализация базы:
     * создаем, таблицы, наполняем их если надо
     *
     * @param sqLiteDatabase объект БД, который имеет к ней доступ
     */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(UserFilesDAO.SQL_CREATE_TABLE);
        sqLiteDatabase.execSQL("INSERT INTO user_data (author_name, file_path, storage_type) VALUES (\"Denis\", \"/file/\", \"INTERNAL\")");
        sqLiteDatabase.execSQL("INSERT INTO user_data (author_name, file_path, storage_type) VALUES (\"Denis\", \"/file/\", \"INTERNAL\")");
        sqLiteDatabase.execSQL("INSERT INTO user_data (author_name, file_path, storage_type) VALUES (\"Denis\", \"/file/\", \"INTERNAL\")");
        sqLiteDatabase.execSQL("INSERT INTO user_data (author_name, file_path, storage_type) VALUES (\"Alex\", \"/file/\", \"INTERNAL\")");
        sqLiteDatabase.execSQL("INSERT INTO user_data (author_name, file_path, storage_type) VALUES (\"Alex\", \"/file/\", \"INTERNAL\")");
        sqLiteDatabase.execSQL("INSERT INTO user_data (author_name, file_path, storage_type) VALUES (\"Alex\", \"/file/\", \"INTERNAL\")");
        sqLiteDatabase.execSQL("INSERT INTO user_data (author_name, file_path, storage_type) VALUES (\"Alex\", \"/file/\", \"INTERNAL\")");
        sqLiteDatabase.execSQL("INSERT INTO user_data (author_name, file_path, storage_type) VALUES (\"Alex\", \"/file/\", \"INTERNAL\")");
        sqLiteDatabase.execSQL("INSERT INTO user_data (author_name, file_path, storage_type) VALUES (\"Alex\", \"/file/\", \"INTERNAL\")");
        sqLiteDatabase.execSQL("INSERT INTO user_data (author_name, file_path, storage_type) VALUES (\"Alex\", \"/file/\", \"INTERNAL\")");
    }

    /**
     * Вызывается если увеличилась версия базы данных
     *
     * @param sqLiteDatabase ссылка на базу
     * @param oldVersion     номер предыдущей версии базы
     * @param newVersion     новый номер текущей версии базы
     */
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

    }

    @Override
    public synchronized void close() {
        closeUserFilesDAO();
        super.close();
    }

    /**
     * Возвращаем объект для работы с таблицей "user_data"
     * @return Объект с открытым соединением к базе
     */
    public UserFilesDAO getUserFilesDAO(){
        if (userFilesDAO == null) {
            userFilesDAO = new UserFilesDAO(getWritableDatabase());
        }
        return userFilesDAO;
    }

    private void closeUserFilesDAO(){
        if (userFilesDAO != null) {
            userFilesDAO.release();
            userFilesDAO = null;
        }
    }
}
