package by.itacademy.cource.storage.contentprovider;

import android.content.Context;

import androidx.annotation.NonNull;

import by.itacademy.cource.storage.database.openhelper.DBHelper;

class DeleteActionFactory {
    static ContentProviderDelete getAuthorContentProvider(@NonNull Context context) {
        return new AuthorContentProvider(DBHelper.getInstance(context));
    }
}
