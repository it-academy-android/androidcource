package by.itacademy.cource.storage.contentprovider;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import by.itacademy.cource.storage.database.openhelper.DBHelper;
import by.itacademy.cource.storage.database.openhelper.UserFilesDAO;

import static by.itacademy.cource.storage.database.openhelper.UserFilesDAO.COLUMN_ID;

public class AuthorContentProvider implements ContentProviderQuery, ContentProviderInsert, ContentProviderDelete {

    private DBHelper dbHelper;

    AuthorContentProvider(DBHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    @Override
    public Cursor query(@Nullable String[] projection, @Nullable String selection,
                        @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        Cursor cursor = dbHelper.getReadableDatabase().query(UserFilesDAO.TABLE_NAME, projection, selection,
                selectionArgs, null, null, sortOrder);
        return cursor;
    }

    @Override
    public void close() {
//        dbHelper.close();
//        dbHelper = null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        long rowID = dbHelper.getWritableDatabase().insert(UserFilesDAO.TABLE_NAME, null, contentValues);
        return ContentUris.withAppendedId(uri, rowID);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        String id = uri.getLastPathSegment();
        String selection = COLUMN_ID + " = " + id;
        return dbHelper.getWritableDatabase().delete(UserFilesDAO.TABLE_NAME, selection, strings);
    }
}
