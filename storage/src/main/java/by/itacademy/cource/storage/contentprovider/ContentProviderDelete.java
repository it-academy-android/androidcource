package by.itacademy.cource.storage.contentprovider;

import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface ContentProviderDelete extends ContentProviderClosable {
    int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings);
}
