package by.itacademy.cource.storage;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import by.itacademy.cource.storage.database.openhelper.DBHelper;
import by.itacademy.cource.storage.database.openhelper.model.DBSavedFile;

import static by.itacademy.cource.storage.Consts.EXTRAS_USER_NAME;
import static by.itacademy.cource.storage.Consts.EXTRA_FILE_ID;
import static by.itacademy.cource.storage.Consts.EXTRA_FILE_NAME;
import static by.itacademy.cource.storage.Consts.EXTRA_IS_EXT_FILE;
import static com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_SHORT;

public class UserInfoActivity extends AppCompatActivity {

    private static final int EDIT_FILE_REQUEST_CODE = 1001;

    private Dialog dialog;
    private DBHelper dbHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        // получаем ссылку на БД
        dbHelper = DBHelper.getInstance(this);

        initToolbar();
        initFileList();
        initAddFileButton();
    }

    /**
     * Реагируем на нажатие стрелки влево на туллбаре
     */
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    protected void onDestroy() {
        dbHelper = null;
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == EDIT_FILE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            initFileList();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    // Устанавливаем Toolbat в нашу UserInfoActivity и настраиваем заголовок на подобие "Hi Author"
    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getToolbarTitle());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    // Настраиваем наш список
    private void initFileList() {
        RecyclerView recyclerView = findViewById(R.id.fileList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, RecyclerView.VERTICAL));
        recyclerView.setAdapter(new FilesRecycleAdapter(getFileListFromDatabase()));
    }

    // настраиваем кнопку для добавления файла
    private void initAddFileButton() {
        findViewById(R.id.floatingActionButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogToAddFileName();
            }
        });
    }

    // Забираем выбранное имя из аргументов активити и строку из ресурсов и соединяем их
    private String getToolbarTitle() {
        return String.format(getString(R.string.toolbar_title_author), getIntent().getStringExtra(EXTRAS_USER_NAME));
    }

    // Забираем выбранное имя из аргументов активити и запрашиваем по нему список сохраненных файлов
    private List<DBSavedFile> getFileListFromDatabase() {
        String authorName = getIntent().getStringExtra(EXTRAS_USER_NAME);
        return dbHelper.getUserFilesDAO().getUserFilesByAuthorName(authorName);
    }

    // Показываем диалог с просьбой ввести имя файла.
    private void showDialogToAddFileName() {
        final View view = LayoutInflater.from(this).inflate(R.layout.dialog_view, null, false);
        dialog = new AlertDialog.Builder(this)
                .setTitle("Put your file name")
                .setView(view)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Если имя файла не пустое то стартуем следующую активити
                        // Если имя файла пустое то показываем сообщение
                        String fileName = ((EditText) view.findViewById(R.id.fileNameEditText)).getText().toString();
                        boolean isSaveToExtStorage = ((CheckBox)view.findViewById(R.id.saveToExtStorageCheckBox)).isChecked();
                        if (!fileName.isEmpty()) {
                            startEditFileActivityWithFileName(fileName, isSaveToExtStorage);
                        } else {
                            Snackbar.make(view, "The file name can not be empty", LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        dialog.show();
    }

    /**
     * Адаптер для нешего списка
     * https://developer.android.com/guide/topics/ui/layout/recyclerview
     */
    private class FilesRecycleAdapter extends RecyclerView.Adapter<FilesRecycleAdapter.FilesViewHolder> {

        private List<DBSavedFile> itemList;

        FilesRecycleAdapter(List<DBSavedFile> itemList) {
            this.itemList = itemList;
        }

        @NonNull
        @Override
        public FilesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_file_data, parent, false);
            return new FilesViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull FilesViewHolder holder, int position) {
            holder.setData(itemList.get(position));
        }

        @Override
        public int getItemCount() {
            return itemList != null ? itemList.size() : 0;
        }

        private class FilesViewHolder extends RecyclerView.ViewHolder {

            private TextView textView;

            FilesViewHolder(@NonNull View itemView) {
                super(itemView);
                textView = itemView.findViewById(R.id.filePathText);
            }

            private void setData(final DBSavedFile item) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startEditFileActivityWithFileId(item.getId());
                    }
                });
                textView.setText(item.getFilePath());
            }
        }
    }

    // Стартуем EditFileActivity и устанавливаем в аргументы идентификатор файла в базе
    private void startEditFileActivityWithFileId(int fileId) {
        Intent intent = new Intent(this, EditFileActivity.class);
        intent.putExtra(EXTRA_FILE_ID, fileId);
        startActivityForResult(intent, EDIT_FILE_REQUEST_CODE);
    }

    // Стартуем EditFileActivity и устанавливаем в аргументы название файла, который нужно будет создать
    private void startEditFileActivityWithFileName(String fileName, boolean isSaveToExtStorage) {
        Intent intent = new Intent(this, EditFileActivity.class);
        intent.putExtra(EXTRA_FILE_NAME, fileName);
        intent.putExtra(EXTRA_IS_EXT_FILE, isSaveToExtStorage);
        intent.putExtra(EXTRAS_USER_NAME, getIntent().getStringExtra(EXTRAS_USER_NAME));
        startActivityForResult(intent, EDIT_FILE_REQUEST_CODE);
    }

}
