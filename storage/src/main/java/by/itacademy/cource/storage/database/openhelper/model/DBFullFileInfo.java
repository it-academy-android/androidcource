package by.itacademy.cource.storage.database.openhelper.model;

/**
 * Класс, который хранит данные, которыми будут поставляться в базу для записи, удаления и для выборки из базы
 * Поля повторяют столбцы из нашей таблицы "user_data"
 */
public class DBFullFileInfo implements DatabaseModel {

    /**
     * Данные базы. СМ {@see UserFilesDAO }
     */
    private int id;
    private String authorName;
    private String filePath;
    private StorageType storageType;

    public DBFullFileInfo() {
    }

    public DBFullFileInfo(int id, String authorName, String filePath, StorageType storageType) {
        this.id = id;
        this.authorName = authorName;
        this.filePath = filePath;
        this.storageType = storageType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public StorageType getStorageType() {
        return storageType;
    }

    public void setStorageType(StorageType storageType) {
        this.storageType = storageType;
    }
}