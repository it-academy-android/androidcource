package by.itacademy.cource.storage.contentprovider;

import android.content.ContentValues;
import android.net.Uri;

public interface ContentProviderInsert extends ContentProviderClosable {
    Uri insert(Uri uri, ContentValues contentValues);
}
