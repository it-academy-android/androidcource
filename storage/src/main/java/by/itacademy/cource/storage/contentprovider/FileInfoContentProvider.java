package by.itacademy.cource.storage.contentprovider;

import android.database.Cursor;

import androidx.annotation.Nullable;

import by.itacademy.cource.storage.database.openhelper.DBHelper;
import by.itacademy.cource.storage.database.openhelper.UserFilesDAO;

import static by.itacademy.cource.storage.database.openhelper.UserFilesDAO.COLUMN_FILE_PATH;
import static by.itacademy.cource.storage.database.openhelper.UserFilesDAO.COLUMN_STORAGE_TYPE;

public class FileInfoContentProvider implements ContentProviderQuery {

    private DBHelper dbHelper;

    FileInfoContentProvider(DBHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    @Override
    public Cursor query(@Nullable String[] projection, @Nullable String selection,
                        @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        return dbHelper.getReadableDatabase()
                .query(UserFilesDAO.TABLE_NAME, new String[]{COLUMN_FILE_PATH, COLUMN_STORAGE_TYPE},
                        selection, selectionArgs, null, null, sortOrder);
    }

    @Override
    public void close() {
        dbHelper.close();
        dbHelper = null;
    }
}
