package by.itacademy.cource.storage.contentprovider;

import android.content.Context;

import androidx.annotation.NonNull;

import by.itacademy.cource.storage.database.openhelper.DBHelper;

class QueryActionFactory {
    static ContentProviderQuery getAuthorContentProvider(@NonNull Context context) {
        return new AuthorContentProvider(DBHelper.getInstance(context));
    }

    static ContentProviderQuery getFileInfoContentProvider(@NonNull Context context) {
        return new FileInfoContentProvider(DBHelper.getInstance(context));
    }
}
