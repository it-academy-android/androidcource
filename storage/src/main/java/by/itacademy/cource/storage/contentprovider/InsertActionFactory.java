package by.itacademy.cource.storage.contentprovider;

import android.content.Context;

import androidx.annotation.NonNull;

import by.itacademy.cource.storage.database.openhelper.DBHelper;

class InsertActionFactory {
    static ContentProviderInsert getAuthorContentProvider(@NonNull Context context) {
        return new AuthorContentProvider(DBHelper.getInstance(context));
    }
}
