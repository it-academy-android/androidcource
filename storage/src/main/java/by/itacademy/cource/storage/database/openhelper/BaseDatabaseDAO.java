package by.itacademy.cource.storage.database.openhelper;

import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;

import by.itacademy.cource.storage.database.openhelper.model.DatabaseModel;

/**
 * Базовый класс для DAO обхектов, который хранит базовые операции над конкретной таблицей
 *
 * @param <T> Модель, описывающая модель таблицы
 */
public abstract class BaseDatabaseDAO<T extends DatabaseModel> {

    protected SQLiteDatabase sqLiteDatabase;

    BaseDatabaseDAO(@NonNull SQLiteDatabase sqLiteDatabase) {
        this.sqLiteDatabase = sqLiteDatabase;
    }

    /**
     * Отключаемся от базы и освобождаем ресурсы
     */
    void release() {
        if (isOpenConnection()) {
            sqLiteDatabase.close();
            sqLiteDatabase = null;
        }
    }

    private boolean isOpenConnection() {
        return sqLiteDatabase != null && sqLiteDatabase.isOpen();
    }
}
