package by.itacademy.cource.storage;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Locale;

import static by.itacademy.cource.storage.Consts.EXTRAS_USER_NAME;

/**
 * Активити содержит пример работы с SharedPreferences
 * https://developer.android.com/training/data-storage/shared-preferences
 */
public class MainActivity extends AppCompatActivity {

    // Ключ для получения сохраненного имени из преференсов
    private static final String KEY_USER_NAME = "KEY_USER_NAME";
    // Ключ для получения сохраненного времени сессии из преференсов
    private static final String KEY_LAST_SESSION = "KEY_LAST_SESSION";

    private EditText viewUserNameEditText;

    // Объект, имеющий доступ в хранилищу(файлу) преференсов
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // получаем ссылку на объект, работающий с хранилищем
        sharedPreferences = getPreferences(MODE_PRIVATE);

        initSaveButton();
        initUserNameEditText();
        initLastSessionTextView();
    }

    @Override
    protected void onDestroy() {
        // Получаем введенное имя и если оно не пустое, то сохраняем его в преференсах
        String userName = viewUserNameEditText.getText().toString();
        if (!userName.trim().isEmpty()) {
            saveTypedName(viewUserNameEditText.getText().toString());
        }
        saveSessionTime();
        super.onDestroy();
    }

    /**
     * Настраиваем наше текстовое поле
     */
    private void initUserNameEditText(){
        // находим EditText в разметке по ID
        viewUserNameEditText = findViewById(R.id.viewUserNameEditText);
        viewUserNameEditText.setText(getSavedUserName());
    }

    /**
     * Настраиваем нашу кнопку
     */
    private void initSaveButton(){
        // находим кнопку в разметке по ID
        Button viewSaveUserButton = findViewById(R.id.viewSaveUserButton);

        // Получаем последнее сохраненное имя и если оно пустое то пишем на кнопке "Sign UP", иначе - "Sign In"
        // Строки берутся из ресурсов (strings.xml)
        int buttonTextId = getSavedUserName().isEmpty() ? R.string.text_button_sign_up : R.string.text_button_sign_in;
        viewSaveUserButton.setText(buttonTextId);

        // стартуем вторую активити по нажатию на кнопку
        viewSaveUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Получаем введенное имя и если оно не пустое, то запускаем UserInfoActivity
                String userName = viewUserNameEditText.getText().toString();
                if (!userName.trim().isEmpty()) {
                    startUserInfoActivity(userName);
                }
            }
        });
    }

    /**
     * Настраиваем наше поле, отображающее время послдней сессии
     */
    private void initLastSessionTextView(){
        TextView viewLastSessionTextView = findViewById(R.id.viewLastSessionTextView);

        // Получаем сохраненное время сессии
        long lastSessionTime = getLastSessionTime();
        int lastSessionTimeMins = getLastSessionTimeInMins(lastSessionTime);

        // Если сессия не сохранена или равна 0 то скрываем поле
        if (lastSessionTimeMins == 0) {
            viewLastSessionTextView.setVisibility(View.INVISIBLE);
        }

        // Форматируем строку из strings.xml чтобы получился нормальное тестовое сообщение
        // Получаем строку из strings.xml
        String resText = getString(R.string.text_last_session);
        // Подставляем значение в минутах вместо плейсхолдера в строке
        String formattedText = String.format(resText, lastSessionTimeMins);
        // Устанавливаем формативаронный текст в наш лейбл
        viewLastSessionTextView.setText(formattedText);
    }

    /**
     * получаем текущее время, для того чтобы посчитать
     * как давно была предыдущая сессия в минутах
     */
    private int getLastSessionTimeInMins(long lastSessionTime){
        long currentTimeMillis = Calendar.getInstance(Locale.getDefault()).getTimeInMillis();
        long substructedTimeMillis = currentTimeMillis - lastSessionTime;
        return (int) (substructedTimeMillis / (1000 * 60)); // 1мин = 60000мс
    }

    /**
     * Получаем последнее сохраненное имя по ключу.
     * Если значения по ключу нет, то возвращаем пустую строку
     */
    private String getSavedUserName() {
        return sharedPreferences.getString(KEY_USER_NAME, "");
    }

    /**
     * Получаем сохраненное время последней сессии
     */
    private long getLastSessionTime(){
        return sharedPreferences.getLong(KEY_LAST_SESSION, 0L);
    }

    /**
     * Сохраняем введенное имя в хранилище(файле) преверенсов
     */
    private void saveTypedName(String userName){
        // Создаем редактор преференсов для внесения изменений
        SharedPreferences.Editor editor = sharedPreferences.edit();
        // указываем редактору что мы хотим сохранить/изменить
        editor.putString(KEY_USER_NAME, userName);
        // сохраням значение асинхронно. В память записывается сразу, в файл - асинхронно.
        // для синхронной записи нужно вызвать команду commit()
        editor.apply();
    }

    /**
     * Сохраняем дату и время последней сессии
     */
    private void saveSessionTime(){
        // Получаем текущую дату и время из календаря
        long lastSessionTimeMillis = Calendar.getInstance(Locale.getDefault()).getTimeInMillis();
        // сохраняем время в преференсах по аналогии с введенным имененем
        sharedPreferences.edit().putLong(KEY_LAST_SESSION, lastSessionTimeMillis).apply();
    }

    /**
     * Запускаем активити и устанавливаем введенное имя как параметр
     */
    private void startUserInfoActivity(String userName){
        Intent intent = new Intent(MainActivity.this, UserInfoActivity.class);
        intent.putExtra(EXTRAS_USER_NAME, userName);
        startActivity(intent);
    }
}
