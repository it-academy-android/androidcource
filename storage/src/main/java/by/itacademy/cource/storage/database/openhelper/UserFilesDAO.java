package by.itacademy.cource.storage.database.openhelper;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import by.itacademy.cource.storage.database.openhelper.model.DBFullFileInfo;
import by.itacademy.cource.storage.database.openhelper.model.DBSavedFile;
import by.itacademy.cource.storage.database.openhelper.model.StorageType;

/**
 * Примитивный объект дающий возможность работы с данными базы.
 * Имеет всю информацию о какой-то определенной таблице и все возможные операции над ней
 *
 * https://ru.wikipedia.org/wiki/Data_Access_Object
 */
public class UserFilesDAO extends BaseDatabaseDAO<DBFullFileInfo> {

    public static final String TABLE_NAME = "user_data"; // название таблицы
    public static final String COLUMN_ID = "id"; // столбец с идентификаторами
    public static final String COLUMN_AUTHOR_NAME = "author_name"; // столбец с именем автора файла
    public static final String COLUMN_FILE_PATH = "file_path"; // путь к файлу
    public static final String COLUMN_STORAGE_TYPE = "storage_type"; // тип хранилища (INTERNAL, EXTERNAL)

    // SQL-запрос на создание таблицы "user_data" со всеми нужными столбцами
    static final String SQL_CREATE_TABLE = String.format(
        "CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s TEXT)",
        TABLE_NAME,
        COLUMN_ID,
        COLUMN_AUTHOR_NAME,
        COLUMN_FILE_PATH,
        COLUMN_STORAGE_TYPE
    );

    UserFilesDAO(@NonNull SQLiteDatabase sqLiteDatabase) {
        super(sqLiteDatabase);
    }

    /**
     * Получаем писок авторов из списка сохраненных файлов.
     * Пример выборки через "сырой" запрос
     *
     * @return спиок авторов
     */
    @NonNull
    public List<String> getAuthorNameList() {
        // Формирует строку с SQL запросом на выборку
        String sqlRequest = String.format("SELECT %s FROM %s GROUP BY %s", COLUMN_AUTHOR_NAME, TABLE_NAME, COLUMN_AUTHOR_NAME);

        // Посылаем запрос на выборку
        // Результат возвращается в объекте Cursor. По сути н хранит результат в виде плоской таблицы
        // https://developer.android.com/reference/android/database/sqlite/SQLiteCursor
        Cursor cursor = sqLiteDatabase.rawQuery(sqlRequest, null);

        ArrayList<String> resultList = new ArrayList<String>();
        if (cursor.moveToFirst()) {
            // Получаем индексы столбцов результирующих данных
            int indexName = cursor.getColumnIndex(COLUMN_AUTHOR_NAME);
            do {
                // Забираем данные из курсора по номеру(индексу) столбца
                resultList.add(cursor.getString(indexName));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return resultList;
    }

    /**
     * Получаем данные о файле по имени автора файла.
     * Пример выборки через соответствующий метод объекта sqLiteDatabase
     *
     * @param authorName имя автора
     * @return спиок данных о файлах автора
     */
    @NonNull
    public List<DBSavedFile> getUserFilesByAuthorName(@NonNull String authorName) {

        // Посылаем запрос на выборку
        // Результат возвращается в объекте Cursor. По сути н хранит результат в виде плоской таблицы
        // https://developer.android.com/reference/android/database/sqlite/SQLiteCursor
        Cursor cursor = sqLiteDatabase.query(
            TABLE_NAME,
            new String[]{COLUMN_ID, COLUMN_FILE_PATH, COLUMN_STORAGE_TYPE},
            COLUMN_AUTHOR_NAME + "=?",
            new String[]{authorName},
            null,
            null,
            COLUMN_STORAGE_TYPE
        );

        ArrayList<DBSavedFile> resultList = new ArrayList<DBSavedFile>();
        if (cursor.moveToFirst()) {

            // Получаем индексы столбцов результирующих данных
            int indexID = cursor.getColumnIndex(COLUMN_ID);
            int indexFilePath = cursor.getColumnIndex(COLUMN_FILE_PATH);
            int indexStorageType = cursor.getColumnIndex(COLUMN_STORAGE_TYPE);

            do {
                // Забираем данные из курсора по номеру(индексу) столбца
                DBSavedFile dbSavedFile = new DBSavedFile(
                    cursor.getInt(indexID),
                    cursor.getString(indexFilePath),
                    StorageType.fromDatabase(cursor.getString(indexStorageType))
                );

                resultList.add(dbSavedFile);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return resultList;
    }

    /**
     * Получаем данные о файле по его ID.
     * Пример выборки через соответствующий метод объекта sqLiteDatabase
     *
     * @param fileId идентификатор файла
     * @return спиок данных о файлах автора
     */
    @Nullable
    public DBFullFileInfo getUserFilesByFileId(int fileId) {

        // Формирует строку с SQL запросом на выборку
        String sqlRequest = String.format(
            "SELECT * FROM %s WHERE %s = %s",
            TABLE_NAME,
            COLUMN_ID,
            fileId
        );

        // Посылаем запрос на выборку
        // Результат возвращается в объекте Cursor. По сути н хранит результат в виде плоской таблицы
        // https://developer.android.com/reference/android/database/sqlite/SQLiteCursor
        Cursor cursor = sqLiteDatabase.rawQuery(sqlRequest, null);

        DBFullFileInfo fullFileInfo = null;
        if (cursor.moveToFirst()) {
            // Получаем индексы столбцов результирующих данных
            int indexID = cursor.getColumnIndex(COLUMN_ID);
            int indexName = cursor.getColumnIndex(COLUMN_AUTHOR_NAME);
            int indexFilePath = cursor.getColumnIndex(COLUMN_FILE_PATH);
            int indexStorageType = cursor.getColumnIndex(COLUMN_STORAGE_TYPE);

            fullFileInfo = new DBFullFileInfo(
                cursor.getInt(indexID),
                cursor.getString(indexName),
                cursor.getString(indexFilePath),
                StorageType.fromDatabase(cursor.getString(indexStorageType))
            );
        }
        cursor.close();

        return fullFileInfo;
    }

    public long insert(@NonNull DBFullFileInfo databaseModel) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_AUTHOR_NAME, databaseModel.getAuthorName());
        values.put(COLUMN_FILE_PATH, databaseModel.getFilePath());
        values.put(COLUMN_STORAGE_TYPE, databaseModel.getStorageType().toDatabase());
        return sqLiteDatabase.insert(TABLE_NAME, null, values);
    }
}
