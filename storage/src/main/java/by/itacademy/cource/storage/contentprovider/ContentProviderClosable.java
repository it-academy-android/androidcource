package by.itacademy.cource.storage.contentprovider;

public interface ContentProviderClosable {
    void close();
}
