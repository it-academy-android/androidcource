package by.itacademy.cource.storage.contentprovider;

import android.database.Cursor;

import androidx.annotation.Nullable;

public interface ContentProviderQuery extends ContentProviderClosable{
    Cursor query(@Nullable String[] projection, @Nullable String selection,
                 @Nullable String[] selectionArgs, @Nullable String sortOrder);
}
