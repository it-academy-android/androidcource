package by.itacademy.cource.storage.contentprovider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DataContentProvider extends ContentProvider {

    private static final String AUTHORITY = "by.itacademy.cource.storage";

    private static final int KEY_DATA_ALL = 1;
    private static final int KEY_DATA_ALL_ANY = 3;
    private static final int KEY_FILE_INFO = 2;

    private static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, "DATA/ALL", KEY_DATA_ALL);
        uriMatcher.addURI(AUTHORITY, "DATA/#", KEY_DATA_ALL_ANY);
        uriMatcher.addURI(AUTHORITY, "DATA/FILE_INFO", KEY_FILE_INFO);
    }

    @Override
    public boolean onCreate() {
        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection,
                        @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        ContentProviderQuery contentProviderQuery = null;
        Context context = getContext();
        if (context != null) {
            switch (uriMatcher.match(uri)) {
                case KEY_DATA_ALL:
                    contentProviderQuery = QueryActionFactory.getAuthorContentProvider(context);
                    break;
                case KEY_FILE_INFO:
                    contentProviderQuery = QueryActionFactory.getFileInfoContentProvider(context);
                default:
                    break;
            }
        }

        if (contentProviderQuery != null) {
            Cursor cursor = contentProviderQuery.query(projection, selection, selectionArgs, sortOrder);
            contentProviderQuery.close();
            return cursor;
        }

        return null;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        Uri resultUri = null;
        if (uriMatcher.match(uri) == KEY_DATA_ALL) {
            resultUri = InsertActionFactory.getAuthorContentProvider(getContext()).insert(uri, contentValues);
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return resultUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        int result = 0;
        if (uriMatcher.match(uri) == KEY_DATA_ALL_ANY) {
            result = DeleteActionFactory.getAuthorContentProvider(getContext()).delete(uri, s, strings);
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return result;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }
}
