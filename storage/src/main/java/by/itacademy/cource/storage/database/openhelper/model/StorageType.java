package by.itacademy.cource.storage.database.openhelper.model;

public enum StorageType {

    INTERNAL, EXTERNAL;

    /**
     * SQLite не умеет работать с Enum, поэтому мы конвертируем его в строку для того, чтобы положить в базу
     * @return Строка, готовая к записи в базу
     */
    public String toDatabase() {
        return name().toUpperCase();
    }

    /**
     * SQLite не умеет работать с Enum, так как мы конвертировали его значение в строку, то мы должны его восставоить из данных(строки),
     * которая пришла к нам из базы данных
     * @param dbData строка из базы
     * @return StorageType восстановленный из пришедшей строки
     */
    public static StorageType fromDatabase(String dbData){
        if (EXTERNAL.name().toUpperCase().equals(dbData)){
            return EXTERNAL;
        } else {
            return INTERNAL;
        }
    }

}
