package by.itacademy.cource.storage;

class Consts {
    static final String EXTRAS_USER_NAME = "EXTRAS_USER_NAME";
    static final String EXTRA_FILE_ID = "EXTRA_FILE_ID";
    static final String EXTRA_FILE_NAME = "EXTRA_FILE_NAME";
    static final String EXTRA_IS_EXT_FILE = "EXTRA_IS_EXT_FILE";
    private Consts(){}
}
