package by.itacademy.cource.storage;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;

import by.itacademy.cource.storage.database.openhelper.DBHelper;
import by.itacademy.cource.storage.database.openhelper.model.DBFullFileInfo;

import static by.itacademy.cource.storage.Consts.EXTRAS_USER_NAME;
import static by.itacademy.cource.storage.Consts.EXTRA_FILE_ID;
import static by.itacademy.cource.storage.Consts.EXTRA_FILE_NAME;
import static by.itacademy.cource.storage.Consts.EXTRA_IS_EXT_FILE;
import static by.itacademy.cource.storage.database.openhelper.model.StorageType.EXTERNAL;
import static by.itacademy.cource.storage.database.openhelper.model.StorageType.INTERNAL;

public class EditFileActivity extends AppCompatActivity {

    private DBHelper dbHelper;
    private DBFullFileInfo dbFullFileInfo;

    private EditText editText;

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_file);

        dbHelper = DBHelper.getInstance(this);
        dbFullFileInfo = getFileData();

        initToolbar();
        initButtonSave();
        editText = findViewById(R.id.editText);
        editText.setText(readFileText());
    }

    @Override
    protected void onDestroy() {
        dbHelper = null;
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                writeToFile();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    // Устанавливаем Toolbat в нашу EditFileActivity и настраиваем заголовок по имени файла
    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getToolbarTitle());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    // Получаем название файла из базы или аргументов активити
    private String getToolbarTitle() {
        String filePath = dbFullFileInfo.getFilePath();
        File file = new File(filePath);
        return file.getName();
    }

    // Находим нашу кнопку и вешаем на нее прослушку чтобы среагировать на нажатие в том
    // случае, когда пользователь хочет соханить текст
    private void initButtonSave() {
        Button button = findViewById(R.id.buttonSaveText);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int permission = ActivityCompat.checkSelfPermission(EditFileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (permission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(EditFileActivity.this, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
                } else {
                    writeToFile();
                }

            }
        });
    }

    private String readFileText(){
        File file = new File(dbFullFileInfo.getFilePath());
        StringBuilder text = new StringBuilder();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
        } catch (IOException e) {
            Log.e("Exception", "File reading failed: " + e.toString());
            Snackbar.make(editText, "File was not read", BaseTransientBottomBar.LENGTH_SHORT).show();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return text.toString();
    }
    private void writeToFile(){
        File file = new File(dbFullFileInfo.getFilePath());
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                Log.e("Exception", "File create failed: " + e.toString());
                e.printStackTrace();
            }
        }

        String textToSave = editText.getText().toString();
        FileOutputStream stream = null;
        try {
            stream = new FileOutputStream(file);
            stream.write(textToSave.getBytes());

            dbHelper.getUserFilesDAO().insert(dbFullFileInfo);
            setResult(RESULT_OK);
        }catch (Exception e) {
            Log.e("Exception", "File write failed: " + e.toString());
            Snackbar.make(editText, "File was not written", BaseTransientBottomBar.LENGTH_SHORT).show();
            setResult(RESULT_CANCELED);
        } finally {
            if (stream!= null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        finish();
    }

    // Получаем данные о файле из БД
    private DBFullFileInfo getFileData() {
        int fileId = getIntent().getIntExtra(EXTRA_FILE_ID, -1);
        if (fileId > 0) {
            return dbHelper.getUserFilesDAO().getUserFilesByFileId(fileId);
        }

        Intent intent = getIntent();

        DBFullFileInfo fullFileInfo = new DBFullFileInfo();
        fullFileInfo.setAuthorName(intent.getStringExtra(EXTRAS_USER_NAME));

        String fileName = File.separator + intent.getStringExtra(EXTRA_FILE_NAME)+".txt";
        if (intent.getBooleanExtra(EXTRA_IS_EXT_FILE, false)) {
//            File extStorage = getExternalFilesDir(null);
            File extStorage = Environment.getExternalStorageDirectory();
            if (extStorage != null) {
                fullFileInfo.setFilePath(extStorage.getAbsolutePath() + fileName);
            }
            fullFileInfo.setStorageType(EXTERNAL);
        } else {
            File internalStorage = getFilesDir();
            if (internalStorage != null) {
                fullFileInfo.setFilePath(internalStorage.getAbsolutePath() + fileName);
            }
            fullFileInfo.setStorageType(INTERNAL);
        }

        return fullFileInfo;
    }
}
