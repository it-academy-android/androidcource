package by.itacademy.cource.storage.database.openhelper.model;

/**
 * Класс, который хранит данные о сохраненном файле
 * Поля повторяют столбцы из нашей таблицы "user_data"
 */
public class DBSavedFile implements DatabaseModel {

    /**
     * Данные базы. СМ {@see UserFilesDAO }
     */
    private int id;
    private String filePath;
    private StorageType storageType;

    public DBSavedFile(int id, String filePath, StorageType storageType) {
        this.id = id;
        this.filePath = filePath;
        this.storageType = storageType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public StorageType getStorageType() {
        return storageType;
    }

    public void setStorageType(StorageType storageType) {
        this.storageType = storageType;
    }
}