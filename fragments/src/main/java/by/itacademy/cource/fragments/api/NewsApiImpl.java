package by.itacademy.cource.fragments.api;

import androidx.annotation.NonNull;

import java.util.Collections;
import java.util.List;

import by.itacademy.cource.fragments.BuildConfig;
import by.itacademy.cource.fragments.api.deserializer.NewsListJsonDeserializer;
import by.itacademy.cource.fragments.model.News;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Класс, содержащий логику отправки запроссов на API.
 * Реализовано в виде простейшего синглтона.
 * <p>
 * https://refactoring.guru/ru/design-patterns/singleton
 * https://habr.com/ru/post/129494/
 */
public final class NewsApiImpl implements NewsApi {

    private static final NewsApiImpl INSTANCE = new NewsApiImpl();

    private static final String API_KEY = BuildConfig.API_KEY;
    private static final String HOST = "https://newsapi.org";

    public static NewsApi getInstance() {
        return INSTANCE;
    }

    private OkHttpClient httpClient = new OkHttpClient(); // клиент, через который отправляются запросы

    /**
     * Отправляем запрос на сервер, чтобы получить спиок актуальных новостей.
     * Затем реализуем обратный вызов через CompletableFutureApiCallback и получаем данные.
     *
     * @param callback Обратный вызов, который возвращает данные обратно в UI для отображения
     */
    @Override
    public void getNewsList(@NonNull final ApiCallback<List<News>> callback) {

        /*
         * Формируем строку для GET запроса на получение списка новостей.
         * Пример запроса см. в документации на сайте сервиса https://newsapi.org
         */
        StringBuilder urlBuilder = new StringBuilder(HOST)
                .append("/v2/top-headlines")
                .append("?")
                .append("country=").append("us")
                .append("&")
                .append("apiKey=").append(API_KEY);

        Request request = new Request.Builder()
                .url(urlBuilder.toString())
                .build();

        /* Создаем объект, способный разобрать Json из ответа и вернуть объекты */
        NewsListJsonDeserializer deserializer = new NewsListJsonDeserializer();

        /* Создаем наш обратный вызов основанный на CompletableFuture*/
        CompletableFutureApiCallback<List<News>> apiCallback = new CompletableFutureApiCallback<>(deserializer);
        apiCallback.exceptionally(throwable -> {
            callback.onFailure(throwable);
            return Collections.emptyList();
        }).thenAccept(callback::onSuccess);

        /* Посылаем запрос на наш API*/
        Call apiCall = httpClient.newCall(request);
        apiCall.enqueue(apiCallback);
    }
}
