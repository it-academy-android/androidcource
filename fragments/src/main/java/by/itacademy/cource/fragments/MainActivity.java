package by.itacademy.cource.fragments;

import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import by.itacademy.cource.fragments.fragments.CloseAppDialogFragment;
import by.itacademy.cource.fragments.fragments.NewsListFragment;
import by.itacademy.cource.fragments.fragments.NewsViewerFragment;
import by.itacademy.cource.fragments.fragments.OnCloseApplicationActionListener;
import by.itacademy.cource.fragments.fragments.OnNewsPreviewActionListener;

public class MainActivity extends AppCompatActivity
        implements OnNewsPreviewActionListener, OnCloseApplicationActionListener {

    @Nullable
    @BindView(R.id.fragmentContainer2)
    FrameLayout fragmentContainer2;

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);
        showNewsListFragment();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    @Override
    public void onOpenNews(String url) {
        if (fragmentContainer2 == null) {
            showNewsViewerFragment(url);
        } else {
            showUrl(url);
        }
    }

    private void showNewsListFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentContainer, NewsListFragment.newInstance(), NewsListFragment.FRAGMENT_TAG)
                .commit();
    }

    private void showNewsViewerFragment(String url) {
        NewsViewerFragment fragment = NewsViewerFragment.newInstance(url);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragmentContainer, fragment, NewsViewerFragment.FRAGMENT_TAG)
                .addToBackStack(null)
                .commit();
    }

    private void showUrl(String url) {
        NewsViewerFragment fragment =
                (NewsViewerFragment) getSupportFragmentManager().findFragmentByTag(NewsViewerFragment.FRAGMENT_TAG);
        if (fragment == null) {
            fragment = NewsViewerFragment.newInstance(url);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragmentContainer2, fragment, NewsViewerFragment.FRAGMENT_TAG)
                    .commit();
        } else {
            fragment.showUrl(url);
        }
    }

    private void showCloseAppDialog() {
        CloseAppDialogFragment.newInstance()
                .show(getSupportFragmentManager(), CloseAppDialogFragment.FRAGMENT_TAG);
    }

    @Override
    public void onCloseApplication() {
        finish();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            super.onBackPressed();
        } else {
            showCloseAppDialog();
        }
    }
}
