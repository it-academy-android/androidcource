package by.itacademy.cource.fragments.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import butterknife.BindView;
import by.itacademy.cource.fragments.R;

public class NewsViewerFragment extends BaseFragment {

    public static final String FRAGMENT_TAG = NewsViewerFragment.class.getName();

    private static final String KEY_URL = "KEY_URL";

    public static NewsViewerFragment newInstance(String url) {
        Bundle bundle = new Bundle();
        bundle.putString(KEY_URL, url);

        NewsViewerFragment fragment = new NewsViewerFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @BindView(R.id.newsWebView)
    WebView newsWebView;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return LayoutInflater.from(getContext()).inflate(R.layout.fragment_news_viewer, container, false);
    }

    @Override
    public void onDestroyView() {
        changeProgressVisibility(View.GONE);
        super.onDestroyView();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initWebView();
        newsWebView.loadUrl(getUrl());
    }

    public void showUrl(String url) {
        newsWebView.loadUrl(url);
    }

    private void initWebView() {
        newsWebView.getSettings().setJavaScriptEnabled(true);
        newsWebView.getSettings().setLoadsImagesAutomatically(true);
        newsWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        newsWebView.setWebChromeClient(new WebChromeClient());
        newsWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(request.getUrl().toString());
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                changeProgressVisibility(View.GONE);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                changeProgressVisibility(View.VISIBLE);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                changeProgressVisibility(View.GONE);
            }
        });
    }

    private void changeProgressVisibility(int visibility) {
        if (progressBar != null) {
            progressBar.setVisibility(visibility);
        }
    }

    private String getUrl() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            return bundle.getString(KEY_URL);
        }
        return "";
    }
}
