package by.itacademy.cource.fragments.api;

import java.util.List;

import by.itacademy.cource.fragments.model.News;

/**
 * Интерфес содержащий методы,
 * через которые мы обращаемся к API из фрагментов или активити
 */
public interface NewsApi {
    void getNewsList(ApiCallback<List<News>> callback);
}
