package by.itacademy.cource.fragments.model;

/**
 * POJO класс, содержащий информацию о новостной статье
 *
 * https://devcolibri.com/unit/%D1%83%D1%80%D0%BE%D0%BA-8-%D1%81%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-pojo-%D0%BE%D0%B1%D1%8A%D0%B5%D0%BA%D1%82%D0%B0-user-%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0-%D1%81-view-%D0%B8%D0%B7-java-2/
 */
public class News {

    private String title; // заголовок статьи
    private String description; // краткое описание статьи
    private String url; // URL-адрес статьи в интернете
    private String imageUrl; // URL-адрес картинки для статьи

    public News(String title, String description, String url, String imageUrl) {
        this.title = title;
        this.description = description;
        this.url = url;
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
