package by.itacademy.cource.fragments.api.deserializer;

import org.json.JSONException;

import androidx.annotation.NonNull;

/**
 * Интерфейс десериализатора. Создан, чтобы унифицировать работу с таким типом объектов.
 * То есть работа будет просизводится не с объектов какого-то класса, а с объектом, реулизующим этот интерфейс.
 * Все обращение к этому объекту будет через методы интерфейса
 *
 * @param <T> Данные, которые вернет десериализатор
 */
public interface Deserializer<T> {
    @NonNull
    T getData(String data) throws JSONException;
}
