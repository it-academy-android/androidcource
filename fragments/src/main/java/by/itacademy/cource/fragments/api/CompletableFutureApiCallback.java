package by.itacademy.cource.fragments.api;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

import by.itacademy.cource.fragments.api.deserializer.Deserializer;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Класс для обратного вызова в okHttp.
 * Был создан для того, чтобы интегрировать Callback из okHttp в CompletableFuture
 *
 * CompletableFuture:
 * https://vertex-academy.com/tutorials/ru/java-8-completablefuture/
 * https://habr.com/ru/post/213319/
 * https://www.codeflow.site/ru/article/java-completablefuture
 *
 * @param <T> Данные, пришедшие из API (https://habr.com/ru/company/sberbank/blog/416413/)
 */
public class CompletableFutureApiCallback<T> extends CompletableFuture<T> implements Callback {

    private Deserializer<T> deserializer; // объект, преобразующий данные из вормата JSON в POJO объекты

    CompletableFutureApiCallback(Deserializer<T> deserializer) {
        this.deserializer = deserializer;
    }

    /**
     * Реагируем на возникающие ошибки при обращении к серверу и прокидываем их в CompletableFuture как ошибку
     *
     * @param call объект из библиотеки okHttp, которые управляем обращением к серверу
     * @param e    Ошибка, полученная при обращении к серверу
     */
    @Override
    public void onFailure(@NotNull Call call, @NotNull IOException e) {
        /*
         * Если пришла ошибка, то прокидываем ее дальше в CompletableFuture,
         * чтобы дальше ее обработать
         */
        super.completeExceptionally(e);
    }

    /**
     * Реагируем на успешное получение ответа от сервера
     *
     * @param call     объект из библиотеки okHttp, которые управляем обращением к серверу
     * @param response объект, содержащий данные об ответе с сервера
     */
    @Override
    public void onResponse(@NotNull Call call, @NotNull Response response) {
        /*
         * Если пришел ответ от сервера, то парсим его и
         * отдаем готовые объекты дальше в CompletableFuture.
         * Иначе прокидываем дальше NULL
         */
        ResponseBody responseBody = response.body();
        if (responseBody != null) {
            getDataFromResponseBody(responseBody);
        } else {
            super.complete(null);
        }

    }

    /**
     * Получаем json из ответа, далее парсим и
     * возвращаем результат уже через CompletableFuture во внешнюю среду
     *
     * @param responseBody объект, содержащий данные, пришедшие от сервера
     */
    private void getDataFromResponseBody(ResponseBody responseBody) {
        try {
            super.complete(deserializer.getData(responseBody.string()));
        } catch (Exception e) {
            super.completeExceptionally(e);
        }
    }
}
