package by.itacademy.cource.fragments.api;

/**
 * Интерфейс для прослушивания асинхронных ответов сервера
 *
 * @param <T> Данные, полученные от сервера
 * https://habr.com/ru/company/sberbank/blog/416413/
 */
public interface ApiCallback<T> {
    void onSuccess(T data);

    void onFailure(Throwable e);
}
