package by.itacademy.cource.fragments.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import by.itacademy.cource.fragments.R;
import by.itacademy.cource.fragments.api.ApiCallback;
import by.itacademy.cource.fragments.api.NewsApiImpl;
import by.itacademy.cource.fragments.model.News;

import static android.widget.GridLayout.HORIZONTAL;

public class NewsListFragment extends BaseFragment {

    public static final String FRAGMENT_TAG = NewsListFragment.class.getName();

    public static NewsListFragment newInstance() {
        return new NewsListFragment();
    }

    @BindView(R.id.newsRecyclerView)
    RecyclerView newsRecyclerView;

    private OnNewsPreviewActionListener onNewsPreviewActionListener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnNewsPreviewActionListener) {
            onNewsPreviewActionListener = (OnNewsPreviewActionListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return LayoutInflater.from(getContext()).inflate(R.layout.fragment_news_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initNewsList();
        getNews();
    }

    private void initNewsList() {
        Context context = getContext();
        if (context != null) {
            newsRecyclerView.setAdapter(new NewsListAdapter());
            newsRecyclerView.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
            newsRecyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        }
    }

    private void getNews() {
        NewsApiImpl.getInstance().getNewsList(new ApiCallback<List<News>>() {
            @Override
            public void onSuccess(List<News> data) {
                if (data != null && !data.isEmpty() && newsRecyclerView.getAdapter() != null) {
                    ((NewsListAdapter) newsRecyclerView.getAdapter()).setItemList(data);
                }
            }

            @Override
            public void onFailure(Throwable e) {
                View view = getView();
                if (view != null) {
                    Snackbar.make(view, getString(R.string.errorNewsList), BaseTransientBottomBar.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void openNews(String url) {
        if (onNewsPreviewActionListener != null) {
            onNewsPreviewActionListener.onOpenNews(url);
        }
    }

    class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.NewsItemViewHolder> {

        private List<News> itemList;

        @NonNull
        @Override
        public NewsItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.item_news_preview, parent, false);
            return new NewsItemViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull NewsItemViewHolder holder, int position) {
            holder.bindNews(itemList.get(position));
        }

        @Override
        public int getItemCount() {
            return itemList != null ? itemList.size() : 0;
        }

        void setItemList(List<News> itemList) {
            this.itemList = itemList;
            notifyDataSetChanged();
        }

        class NewsItemViewHolder extends RecyclerView.ViewHolder {

            @BindView(R.id.viewNewsPreviewImage)
            ImageView viewNewsPreviewImage;

            @BindView(R.id.viewNewsDescription)
            TextView viewNewsTextDescription;

            NewsItemViewHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            void bindNews(final News news) {
                viewNewsTextDescription.setText(news.getTitle());
                Glide.with(itemView).load(news.getImageUrl()).into(viewNewsPreviewImage);
                itemView.setOnClickListener(v -> openNews(news.getUrl()));
            }
        }
    }
}
