package by.itacademy.cource.fragments.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import by.itacademy.cource.fragments.R;

/**
 * DialogFragment, показывающий диалог с вопросов о закрытии приложения
 */
public class CloseAppDialogFragment extends DialogFragment {

    public static final String FRAGMENT_TAG = CloseAppDialogFragment.class.getName();

    public static CloseAppDialogFragment newInstance() {
        return new CloseAppDialogFragment();
    }

    private OnCloseApplicationActionListener onCloseApplicationActionListener;

    private DialogInterface.OnClickListener onPositiveClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            if (onCloseApplicationActionListener != null) {
                onCloseApplicationActionListener.onCloseApplication();
            }
        }
    };

    private DialogInterface.OnClickListener onNegativeClickListener = (dialog, which) -> dialog.dismiss();

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnCloseApplicationActionListener) {
            onCloseApplicationActionListener = (OnCloseApplicationActionListener) context;
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.closeAppTitle))
                .setMessage(getString(R.string.closeAppMessage))
                .setPositiveButton(R.string.close, onPositiveClickListener)
                .setNegativeButton(R.string.cancel, onNegativeClickListener)
                .setCancelable(false)
                .create();
    }
}
