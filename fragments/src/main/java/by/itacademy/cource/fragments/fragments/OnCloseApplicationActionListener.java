package by.itacademy.cource.fragments.fragments;

public interface OnCloseApplicationActionListener {
    void onCloseApplication();
}
