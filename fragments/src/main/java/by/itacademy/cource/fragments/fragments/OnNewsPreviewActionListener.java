package by.itacademy.cource.fragments.fragments;

public interface OnNewsPreviewActionListener {
    void onOpenNews(String url);
}
