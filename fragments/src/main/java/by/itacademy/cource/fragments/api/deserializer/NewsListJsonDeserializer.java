package by.itacademy.cource.fragments.api.deserializer;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import by.itacademy.cource.fragments.model.News;

/**
 * Класс, объект которого будет принимать данные в формате Json и возвращать данные в виде объектов
 * Реализует интерфейс Deserializer, чтобы передавать его непосредственно в CompletableFutureApiCallback
 */
public class NewsListJsonDeserializer implements Deserializer<List<News>> {

    /**
     * Реализлванный метод интерфейса
     *
     * @param data Данные, полученные из API в виде строки в формате JSON
     * @return Список новостей в виде объектов
     * @throws JSONException Ошибка, возникающая при парсинге Json
     */
    @NonNull
    @Override
    public List<News> getData(String data) throws JSONException {

        /* Если данные пустые, то мы возвращаем пустой список*/
        if (data == null || data.trim().isEmpty()) return new ArrayList<>();

        JSONObject jsonObject = new JSONObject(data); // Парсим весь json
        return getNewsList(jsonObject.getJSONArray("articles"));
    }

    /**
     * Парсим список стаей из Json
     *
     * @param jsonArticles список стаей в формате Json
     * @return Список объектов, содержащих информацию о статьях
     * @throws JSONException Ошибка, возникающая при парсинге Json
     */
    private List<News> getNewsList(JSONArray jsonArticles) throws JSONException {
        ArrayList<News> newsList = new ArrayList<>();
        if (jsonArticles != null && jsonArticles.length() > 0) {
            for (int i = 0; i < jsonArticles.length(); i++) {
                newsList.add(getNewsFromJson(jsonArticles.getJSONObject(i)));
            }
        }
        return newsList;
    }

    /**
     * Парсим данные конкретной статьи и кладем их в объект
     *
     * @param jsonNews статья в формате Json
     * @return объект, содержащий информацию о статье
     * @throws JSONException Ошибка, возникающая при парсинге Json
     */
    private News getNewsFromJson(JSONObject jsonNews) throws JSONException {
        return new News(
                jsonNews.getString("title"),
                jsonNews.getString("description"),
                jsonNews.getString("url"),
                jsonNews.getString("urlToImage")
        );
    }
}
