package by.itacademy.cource.animation;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragmentContainer, new Fragment1(), Fragment1.class.getName())
                .commit();

        findViewById(R.id.viewStartAnimationButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animatedXMLFade();
                animatedXMLTranslationFade();
                animatedXMLBlink();
                animatedXMLScale();
                startValueAnimatorFade();
                startObjectAnimatorTranslate();
                startAnimationSet();
                startResourceAnimator();
                startViewAlphaAnimation();
                replacefragment();
            }
        });

        findViewById(R.id.viewStartActivity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, MainActivity2.class));
            }
        });
    }

    private void animatedXMLFade() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.fade);
        ImageView imageView = findViewById(R.id.imageFade);
        imageView.startAnimation(animation);
    }

    private void animatedXMLTranslationFade() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.transition_fade);
        ImageView imageView = findViewById(R.id.imageTranslateFade);
        imageView.startAnimation(animation);
    }

    private void animatedXMLBlink() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.blink);
        ImageView imageView = findViewById(R.id.imageBlink);
        imageView.startAnimation(animation);
    }

    private void animatedXMLScale() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.scale);
        ImageView imageView = findViewById(R.id.imageScale);
        imageView.startAnimation(animation);
    }

    private void startValueAnimatorFade() {
        final ImageView imageView = findViewById(R.id.imageValueAnimatorFade);
        ValueAnimator animator = ValueAnimator.ofFloat(.0f, 1.0f);
        animator.setDuration(2000);
        animator.setInterpolator(new LinearInterpolator());
        animator.setRepeatMode(ValueAnimator.RESTART);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float animatedValue = (float) valueAnimator.getAnimatedValue();
                imageView.setAlpha(animatedValue);
            }
        });
        animator.start();
    }

    private void startObjectAnimatorTranslate() {
        final ImageView imageView = findViewById(R.id.imageObjectAnimatorTranslate);
        ObjectAnimator animation = ObjectAnimator.ofFloat(imageView, "translationX", 500.0f, 0.0f);
        animation.setDuration(1000);
        animation.start();
    }

    private void startAnimationSet() {
        final ImageView imageView = findViewById(R.id.imageAnimationSet);

        final AnimatorSet set = new AnimatorSet();

        ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(imageView, "alpha", 0f, 1f);
        alphaAnimator.setDuration(500);
        alphaAnimator.setRepeatCount(Animation.INFINITE);
        alphaAnimator.setRepeatMode(ValueAnimator.REVERSE);

        ObjectAnimator translationAnimator = ObjectAnimator.ofFloat(imageView, "translationX", 500.0f, 0.0f);
        translationAnimator.setDuration(2000);

        set.playTogether(alphaAnimator, translationAnimator);
        set.start();
    }

    private void startResourceAnimator() {
        final ImageView imageView = findViewById(R.id.imageResourceAnimator);
        AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.animator);
        set.setTarget(imageView);
        set.start();
    }

    private void startViewAlphaAnimation() {
        final ImageView imageView = findViewById(R.id.imageAnimatedAlpha);
        imageView.setAlpha(.0f);
        imageView.setScaleX(.0f);
        imageView.setScaleY(.0f);
        imageView.animate()
                .alpha(1.0f)
                .scaleX(1.0f)
                .scaleY(1.0f)
                .setDuration(2000)
                .start();
    }

    private void replacefragment() {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_right_in, R.anim.slide_left_out,
                        R.anim.slide_left_in, R.anim.slide_right_out)
                .replace(R.id.fragmentContainer, new Fragment2(), Fragment2.class.getName())
                .addToBackStack(null)
                .commit();
    }

}
