package by.itacademy.cource.animation;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out);
        setContentView(R.layout.activity_main2);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragmentContainer, new Fragment1(), Fragment1.class.getName())
                .commit();
    }
}
