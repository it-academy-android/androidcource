package by.itacademy.cource.network;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView tempTextView;
    private TextView descriptionTextView;

    private OkHttpClient client = new OkHttpClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imgWeatherIcon);
        tempTextView = findViewById(R.id.textTemperature);
        descriptionTextView = findViewById(R.id.weatherDescription);

        getForecast();
    }

    private void getForecast() {
        final String url = String.format(Consts.CURRENT_WEATHER_URL, "Minsk", BuildConfig.API_KEY);
        Request request = new Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "The forecast is not available", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) {
                try {
                    Forecast forecast = getForecastFromResponse(response.body());
                    updateForecast(forecast);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private Forecast getForecastFromResponse(@Nullable ResponseBody body) throws IOException {
        return body != null ? new ResponseParser(body.string()).getForecast() : null;
    }

    private void updateForecast(@Nullable final Forecast forecast) {
        if (forecast != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String iconUrl = String.format(Consts.CURRENT_ICON_URL, forecast.getIconId());
                    Picasso.get().load(iconUrl).into(imageView);

                    descriptionTextView.setText(forecast.getDescription());

                    String tempText = String.format(getString(R.string.temp_celcius), forecast.getTemperature());
                    tempTextView.setText(tempText);
                }
            });
        }
    }
}
