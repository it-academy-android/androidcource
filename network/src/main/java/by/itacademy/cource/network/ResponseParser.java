package by.itacademy.cource.network;

import org.json.JSONException;
import org.json.JSONObject;

class ResponseParser {

    private String responseBody;

    ResponseParser(String responseBody) {
        this.responseBody = responseBody;
    }

    Forecast getForecast() {
        Forecast.Builder builder = new Forecast.Builder();

        try {
            JSONObject jsonObject = new JSONObject(responseBody);

            builder.setTemperature(jsonObject.getJSONObject("main").getDouble("temp"));
            builder.setDescription(jsonObject.getJSONArray("weather").getJSONObject(0).getString("description"));
            builder.setIconId(jsonObject.getJSONArray("weather").getJSONObject(0).getString("icon"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return builder.build();
    }
}
