package by.itacademy.cource.network;

/**
 * Класс, хранящий константы
 */
class Consts {

    static final String CURRENT_WEATHER_URL = "https://api.openweathermap.org/data/2.5/weather?q=%s&APPID=%s&units=metric";
    static final String CURRENT_ICON_URL = "https://openweathermap.org/img/wn/%s@2x.png";

    /**
     * Обязательно делаем приватны конструктор,
     * чтобы никто не имел возможност инициализировать этот класс
     */
    private Consts() {
    }
}
