package by.itacademy.cource.network;

/**
 * Класс, который мы используем для отображения данных в UI
 */
public class Forecast {

    private double temperature; // Температура
    private String description; // Словесное описание погоды
    private String iconId; // ID Иконки, характеризующей погоду

    private Forecast(double temperature, String description, String iconId) {
        this.temperature = temperature;
        this.description = description;
        this.iconId = iconId;
    }

    public double getTemperature() {
        return temperature;
    }

    public String getDescription() {
        return description;
    }

    public String getIconId() {
        return iconId;
    }

    public static class Builder {
        private double temperature; // Температура
        private String description; // Словесное описание погоды
        private String iconId; // ID Иконки, характеризующей погоду

        public Builder() {
        }

        public Builder setTemperature(double temperature) {
            this.temperature = temperature;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder setIconId(String iconId) {
            this.iconId = iconId;
            return this;
        }

        public Forecast build() {
            return new Forecast(temperature, description, iconId);
        }
    }
}
