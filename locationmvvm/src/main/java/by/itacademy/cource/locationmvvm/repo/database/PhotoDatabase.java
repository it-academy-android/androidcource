package by.itacademy.cource.locationmvvm.repo.database;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {PhotoEntity.class}, version = 1, exportSchema = false)
public abstract class PhotoDatabase extends RoomDatabase {

    private static volatile PhotoDatabase INSTANCE;

    public static PhotoDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (PhotoDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        PhotoDatabase.class, "db_photo.db")
                        .build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract PhotoDao photoDao();
}
