package by.itacademy.cource.locationmvvm.repo.database;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface PhotoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(PhotoEntity word);

    @Query("DELETE FROM photo WHERE id == :id")
    int delete(long id);

    @Query("SELECT * from photo ORDER BY date")
    List<PhotoEntity> getAll();

    @Query("SELECT * from photo WHERE id == :id ORDER BY date")
    PhotoEntity getPhotoEntity(long id);

}
