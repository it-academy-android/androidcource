package by.itacademy.cource.locationmvvm.utils;

import android.location.Location;

import androidx.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;

public class LocationUtils {

    @Nullable
    public static LatLng locationToLatLng(Location location) {
        return location != null ? new LatLng(location.getLatitude(), location.getLongitude()) : null;
    }


    private LocationUtils() {
    }
}
