package by.itacademy.cource.locationmvvm.utils;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import by.itacademy.cource.locationmvvm.maps.MapFragmentViewModel;
import by.itacademy.cource.locationmvvm.maps.PhotoEntityToMarkerOptionsMapper;
import by.itacademy.cource.locationmvvm.photoviewer.PhotoEntityToBitmapMapper;
import by.itacademy.cource.locationmvvm.photoviewer.PhotoViewerFragmentViewModel;
import by.itacademy.cource.locationmvvm.repo.RepositoryImpl;

public class ViewModelFactory implements ViewModelProvider.Factory {

    private Application application;

    public ViewModelFactory(Application application) {
        this.application = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(MapFragmentViewModel.class)) {
            return (T) new MapFragmentViewModel(application, new RepositoryImpl(application),
                    new PhotoEntityToMarkerOptionsMapper());
        } else if (modelClass.isAssignableFrom(PhotoViewerFragmentViewModel.class)) {
            return (T) new PhotoViewerFragmentViewModel(application, new RepositoryImpl(application),
                    new PhotoEntityToBitmapMapper());
        }
        return null;
    }
}
