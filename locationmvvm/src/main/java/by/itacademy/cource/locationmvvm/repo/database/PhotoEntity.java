package by.itacademy.cource.locationmvvm.repo.database;

import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.util.Date;

@Entity(tableName = "photo")
@TypeConverters({Converters.class})
public class PhotoEntity {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @Nullable
    private Date date;

    @Nullable
    private File filePath;

    @Nullable
    private LatLng coordintes;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Nullable
    public Date getDate() {
        return date;
    }

    public void setDate(@Nullable Date date) {
        this.date = date;
    }

    @Nullable
    public File getFilePath() {
        return filePath;
    }

    public void setFilePath(@Nullable File filePath) {
        this.filePath = filePath;
    }

    @Nullable
    public LatLng getCoordintes() {
        return coordintes;
    }

    public void setCoordintes(@Nullable LatLng coordintes) {
        this.coordintes = coordintes;
    }
}
