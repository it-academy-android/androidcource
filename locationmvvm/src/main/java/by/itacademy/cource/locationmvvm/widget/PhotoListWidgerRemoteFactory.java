package by.itacademy.cource.locationmvvm.widget;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import by.itacademy.cource.locationmvvm.R;
import by.itacademy.cource.locationmvvm.repo.database.PhotoDao;
import by.itacademy.cource.locationmvvm.repo.database.PhotoDatabase;
import by.itacademy.cource.locationmvvm.repo.database.PhotoEntity;
import by.itacademy.cource.locationmvvm.utils.ImageUtils;

public class PhotoListWidgerRemoteFactory implements RemoteViewsService.RemoteViewsFactory {

    private Context context;
    private PhotoDao photoDao;
    private List<PhotoEntity> photoEntityList;

    PhotoListWidgerRemoteFactory(Context context) {
        this.context = context;
    }

    @Override
    public void onCreate() {
        photoDao = PhotoDatabase.getDatabase(context).photoDao();
    }

    @Override
    public void onDataSetChanged() {
        if (context != null) {
            photoEntityList = photoDao.getAll();
        }
    }

    @Override
    public void onDestroy() {
        context = null;
        photoDao = null;
    }

    @Override
    public int getCount() {
        return photoEntityList != null ? photoEntityList.size() : 0;
    }

    @Override
    public RemoteViews getViewAt(int i) {
        PhotoEntity item = photoEntityList.get(i);

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.item_widget_photo);

        LatLng position = item.getCoordintes();
        String positionText = position != null ? position.toString() : "N/A";
        remoteViews.setTextViewText(R.id.widgetViewCoordsText, positionText);

        if (item.getFilePath() != null) {
            Bitmap imageBitmap = ImageUtils.decodeBitmapFromFile(item.getFilePath());
            Bitmap proxy = Bitmap.createBitmap(imageBitmap.getWidth(), imageBitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(proxy);
            c.drawBitmap(imageBitmap, new Matrix(), null);
            remoteViews.setImageViewBitmap(R.id.widgetViewImagePreview, proxy);
        }

        Intent clickIntent = new Intent();
        clickIntent.putExtra("PHOTO_ID", item.getId());
        remoteViews.setOnClickFillInIntent(R.id.widgetViewImagePreview, clickIntent);

        return remoteViews;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int i) {
        return photoEntityList != null ? photoEntityList.get(i).getId() : 0;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }
}
