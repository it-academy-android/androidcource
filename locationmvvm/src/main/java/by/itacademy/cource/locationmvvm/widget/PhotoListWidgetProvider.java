package by.itacademy.cource.locationmvvm.widget;

import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.widget.RemoteViews;

import by.itacademy.cource.locationmvvm.MainActivity;
import by.itacademy.cource.locationmvvm.R;

public class PhotoListWidgetProvider extends AppWidgetProvider {

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        if (appWidgetIds != null && appWidgetIds.length > 0) {
            for (Integer appWidgetId : appWidgetIds) {
                setList(context, appWidgetManager, appWidgetId);
            }
        }
    }

    void setList(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
        Intent adapter = new Intent(context, PhotoWidgetListService.class);
        adapter.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_photo_layout);
        remoteViews.setRemoteAdapter(R.id.widgetViewPhotoList, adapter);

        setListClick(remoteViews, context);

        appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId, R.id.widgetViewPhotoList);
    }

    void setListClick(RemoteViews rv, Context context) {
        Intent listClickIntent = new Intent(context, MainActivity.class);
        listClickIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        listClickIntent.setAction(context.getPackageName() + "onClick");
        PendingIntent listClickPIntent = PendingIntent.getActivity(context, 0, listClickIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        rv.setPendingIntentTemplate(R.id.widgetViewPhotoList, listClickPIntent);
    }
}
