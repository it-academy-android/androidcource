package by.itacademy.cource.locationmvvm.repo;

import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import by.itacademy.cource.locationmvvm.repo.database.PhotoEntity;

public interface Repository {
    CompletableFuture<List<PhotoEntity>> getPhotoList();
    CompletableFuture<PhotoEntity> getPhoto(long id);
    CompletableFuture<Void> savePhoto(LatLng position, File photoFile);
}
