package by.itacademy.cource.locationmvvm.photoviewer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import by.itacademy.cource.locationmvvm.R;
import by.itacademy.cource.locationmvvm.utils.ViewModelFactory;

public class PhotoViewerFragment extends Fragment {

    public static final String TAG = PhotoViewerFragment.class.getName();
    private static final String EXTRA_ID = "EXTRA_ID";

    public static PhotoViewerFragment newInstance(long id) {
        Bundle bundle = new Bundle();
        bundle.putLong(EXTRA_ID, id);
        PhotoViewerFragment fragment = new PhotoViewerFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @BindView(R.id.imageView)
    ImageView imageView;

    private PhotoViewerFragmentViewModel viewerFragmentViewModel;
    private Unbinder unbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewModelFactory factory = new ViewModelFactory(getActivity().getApplication());
        viewerFragmentViewModel = ViewModelProviders.of(this, factory).get(PhotoViewerFragmentViewModel.class);
        viewerFragmentViewModel.getMarkerLiveData().observe(this, bitmap -> imageView.setImageBitmap(bitmap));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_photo_viewer, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        viewerFragmentViewModel.fetchPhoto(getPhotoIdFromArgs());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void updateImage(long id) {
        viewerFragmentViewModel.fetchPhoto(id);
    }

    private long getPhotoIdFromArgs() {
        Bundle bundle = getArguments();
        return bundle != null ? bundle.getLong(EXTRA_ID) : -1;
    }
}
