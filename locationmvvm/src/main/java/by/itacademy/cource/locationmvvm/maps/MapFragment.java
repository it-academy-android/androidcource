package by.itacademy.cource.locationmvvm.maps;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import by.itacademy.cource.locationmvvm.R;
import by.itacademy.cource.locationmvvm.dialog.OkCancelDialogFragment;
import by.itacademy.cource.locationmvvm.utils.CameraUtils;
import by.itacademy.cource.locationmvvm.utils.LocationUtils;
import by.itacademy.cource.locationmvvm.utils.ViewModelFactory;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class MapFragment extends Fragment implements OnMapReadyCallback, OkCancelDialogFragment.OnDialogOkActionListener {

    public interface OnMapActionListener {
        void onOpenPhoto(long id);
    }

    public static final String TAG = MapFragment.class.getName();
    private static final int REQUEST_CODE_LOCATION_PERMISSION = 18000;
    private static final int REQUEST_CODE_CAMERA_PERMISSION = 19000;
    private static final int REQUEST_CODE_TAKE_PICTURE = 20000;

    public static MapFragment newInstance() {
        return new MapFragment();
    }

    @BindView(R.id.viewOpenCamera)
    FloatingActionButton viewOpencamera;

    private MapFragmentViewModel viewModel;
    private LocationManager locationManager;
    private GoogleMap googleMap;
    private Unbinder unbinder;
    private File tempPhotoFile;
    private LatLng userLocation;
    private OnMapActionListener onMapActionListener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnMapActionListener) {
            onMapActionListener = (OnMapActionListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        ViewModelFactory factory = new ViewModelFactory(getActivity().getApplication());
        viewModel = ViewModelProviders.of(this, factory).get(MapFragmentViewModel.class);
        viewModel.getMarkerLiveData().observe(getViewLifecycleOwner(), markerStates -> showMarkers(markerStates));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.supportMapFragment);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMarkerClickListener(marker -> {
            if (onMapActionListener != null) {
                onMapActionListener.onOpenPhoto((Long) marker.getTag());
            }
            return false;
        });
        viewModel.fetchPhotoMarkers();
        checkLocationPermission();
    }

    @Override
    public void onDestroyView() {
        googleMap = null;
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroyView();
    }

    @Override
    public void onOkClick() {
        askLocationPermission();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_LOCATION_PERMISSION && grantResults.length >= 2 &&
                grantResults[0] == PERMISSION_GRANTED && grantResults[1] == PERMISSION_GRANTED) {
            enableMyLocation();
        } else if (requestCode == REQUEST_CODE_CAMERA_PERMISSION && grantResults.length >= 2 &&
                grantResults[0] == PERMISSION_GRANTED && grantResults[1] == PERMISSION_GRANTED) {
            openCamera();
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE_TAKE_PICTURE && resultCode == RESULT_OK) {
            viewModel.savePhoto(tempPhotoFile, userLocation);
        } else {
            if (tempPhotoFile != null && tempPhotoFile.exists()) {
                tempPhotoFile.delete();
            }
            tempPhotoFile = null;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.viewOpenCamera)
    public void onOpenCameraClick() {
        checkCamerapermissions();
    }

    @SuppressLint("MissingPermission")
    private void enableMyLocation() {
        if (googleMap != null) {
            googleMap.setMyLocationEnabled(true);
        }

        userLocation = LocationUtils.locationToLatLng(getLastKnownLocation());
        if (userLocation != null && googleMap != null) {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(userLocation, 10);
            googleMap.animateCamera(cameraUpdate);
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                userLocation = LocationUtils.locationToLatLng(location);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
            }

            @Override
            public void onProviderEnabled(String s) {
            }

            @Override
            public void onProviderDisabled(String s) {
            }
        });
    }

    @SuppressLint("MissingPermission")
    private Location getLastKnownLocation() {
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    private void checkLocationPermission() {
        Context context = getContext();
        if (context != null) {
            if (ContextCompat.checkSelfPermission(getContext(), ACCESS_FINE_LOCATION) == PERMISSION_GRANTED) {
                enableMyLocation();
            } else if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
                OkCancelDialogFragment.newInstance(getString(R.string.location_permission), getString(R.string.location_permission_description))
                        .show(getChildFragmentManager(), OkCancelDialogFragment.TAG);
            } else {
                askLocationPermission();
            }
        }
    }

    private void openCamera() {
        Context context = getContext();
        if (context != null && CameraUtils.hasCameraHardware(context)) {
            try {
                tempPhotoFile = CameraUtils.preparePhotoFile(context);
                Intent takePictureIntent = CameraUtils.prepareCameraIntent(context, tempPhotoFile);
                if (takePictureIntent != null) {
                    startActivityForResult(takePictureIntent, REQUEST_CODE_TAKE_PICTURE);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void checkCamerapermissions() {
        Context context = getContext();
        if (context != null) {
            ArrayList<String> permissions = new ArrayList<>();
            if (ContextCompat.checkSelfPermission(getContext(), CAMERA) != PERMISSION_GRANTED) {
                permissions.add(CAMERA);
            }

            if (ContextCompat.checkSelfPermission(getContext(), WRITE_EXTERNAL_STORAGE) != PERMISSION_GRANTED) {
                permissions.add(WRITE_EXTERNAL_STORAGE);
            }

            if (permissions.isEmpty()) {
                openCamera();
            } else {
                String[] arr = new String[permissions.size()];
                arr = permissions.toArray(arr);
                requestPermissions(arr, REQUEST_CODE_CAMERA_PERMISSION);
            }
        }
    }

    private void askLocationPermission() {
        String[] permissionsArray = new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION};
        requestPermissions(permissionsArray, REQUEST_CODE_LOCATION_PERMISSION);
    }

    private void showMarkers(List<MarkerState> markerOptions) {
        if (googleMap != null) {
            googleMap.clear();
            markerOptions.forEach(sarkerState -> {
                Marker marker = googleMap.addMarker(sarkerState.getMarkerOptions());
                marker.setTag(sarkerState.getPhotoId());
            });
        }
    }
}
