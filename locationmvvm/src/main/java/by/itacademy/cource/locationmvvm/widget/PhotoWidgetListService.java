package by.itacademy.cource.locationmvvm.widget;

import android.content.Intent;
import android.widget.RemoteViewsService;

public class PhotoWidgetListService extends RemoteViewsService {
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new PhotoListWidgerRemoteFactory(getApplicationContext());
    }
}
