package by.itacademy.cource.locationmvvm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import java.util.Calendar;

import by.itacademy.cource.locationmvvm.alarm.AlarmReceiver;
import by.itacademy.cource.locationmvvm.job.MapJobService;
import by.itacademy.cource.locationmvvm.maps.MapFragment;
import by.itacademy.cource.locationmvvm.photoviewer.PhotoViewerFragment;


public class MainActivity extends AppCompatActivity implements MapFragment.OnMapActionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showMapFragment();
        reactOnNewIntent(getIntent());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        scheduleAlarm();
        sheduleJob();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        reactOnNewIntent(intent);
    }

    private void showMapFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentContainer, MapFragment.newInstance(), MapFragment.TAG)
                .commit();
    }

    @Override
    public void onOpenPhoto(long id) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        PhotoViewerFragment fragment = (PhotoViewerFragment) fragmentManager.findFragmentByTag(PhotoViewerFragment.TAG);
        if (fragment == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragmentContainer, PhotoViewerFragment.newInstance(id), PhotoViewerFragment.TAG)
                    .setCustomAnimations(R.anim.slide_from_right_to_left, R.anim.slide_from_left_to_right)
                    .addToBackStack(null)
                    .commit();
        } else {
            fragment.updateImage(id);
        }
    }

    private void reactOnNewIntent(Intent intent) {
        if (intent != null && intent.hasExtra("PHOTO_ID")) {
            onOpenPhoto(intent.getLongExtra("PHOTO_ID", -1));
        }
    }

    private void scheduleAlarm() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        if (alarmManager != null) {
            Intent intent = new Intent(this, AlarmReceiver.class);
            boolean isWorking = PendingIntent.getBroadcast(MainActivity.this, 1000, intent,
                    PendingIntent.FLAG_NO_CREATE) != null;
            if (!isWorking) {
                PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 1000, intent, 0);
                Calendar calendar = Calendar.getInstance();
                alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                        5000, alarmIntent);
            }
        }
    }

    private void sheduleJob() {
        PersistableBundle bundle = new PersistableBundle();
        bundle.putString("TITLE", "JOB TITLE");

        ComponentName serviceName = new ComponentName(this, MapJobService.class);

        JobInfo.Builder jobInfoBuilder = new JobInfo.Builder(1, serviceName)
//                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
//                .setRequiresDeviceIdle(true)
                .setExtras(bundle);
//                .setRequiresCharging(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            jobInfoBuilder.setRequiresBatteryNotLow(true);
        }

        JobScheduler scheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        if (scheduler != null) {
            scheduler.schedule(jobInfoBuilder.build());
        }
    }
}
