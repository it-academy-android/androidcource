package by.itacademy.cource.locationmvvm.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import by.itacademy.cource.locationmvvm.R;

import static android.content.pm.PackageManager.FEATURE_CAMERA;

public class CameraUtils {

    public static boolean hasCameraHardware(@NonNull Context context) {
        return context.getPackageManager().hasSystemFeature(FEATURE_CAMERA);
    }

    public static File preparePhotoFile(@NonNull Context context) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp;
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }

    @Nullable
    public static Intent prepareCameraIntent(@NonNull Context context, @NonNull File photoTempFile){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            Uri photoURI = FileProvider.getUriForFile(context, context.getString(R.string.file_provider_authorities), photoTempFile);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            return takePictureIntent;
        }
        return null;
    }

    private CameraUtils() {
    }
}
