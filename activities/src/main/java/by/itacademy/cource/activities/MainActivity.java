package by.itacademy.cource.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private static final int REQUEST_CODE = 10008;

    private TextView viewResultText;

    private Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // находим TextView в нашем UI, который мы указали в setContentView
        viewResultText = findViewById(R.id.viewResultText);

        // находим кнопку в нашем UI, который мы указали в setContentView
        Button viewStartTimer = findViewById(R.id.viewStartTimer);
        viewStartTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (timer == null) {
                    timer = new Timer();
                }

                // Ставим таймер на 3 секунды и через 3 секунды стартуем другую активити
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        Log.d(TAG, "Timer runs");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // Запуск активити через intent-filter. См. Манифест
                                Intent intent = new Intent("com.example.GET_RESULT");
                                if (intent.resolveActivity(getPackageManager()) != null) {
                                    startActivity(intent);
                                }
                            }
                        });
                    }
                }, 3000);
            }
        });

        Button viewButtonOpenActivity = findViewById(R.id.viewOpenActivity);
        viewButtonOpenActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Запуск активити через явное указание активити в интенте
                Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                // устанавливаем данные, чтобы переедать их в активити
                intent.putExtra("EXTRA_DATA", "Type data here");
                // явно указываем что слудеющая активити может вернуть результат в onActivityResult
                startActivityForResult(intent, REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        // когда активити в процессе оставноки, мы должны очистить наши ресурсы и остановить задачи
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onStop");
        super.onDestroy();
    }

    /**
     * Получаем результат от другой активити. Делаем проверку по requestCode чтобы точно определить
     * какая активити (какой запрос/задача) вернули результат
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK) {
            Log.d(TAG, "requestCode: " + requestCode + " resultCode: RESULT_OK");
        } else if (resultCode == RESULT_CANCELED) {
            Log.d(TAG, "requestCode: " + requestCode + " resultCode: RESULT_CANCELED");
        }


        if (requestCode == REQUEST_CODE && data != null) {
            String returnedData = data.getStringExtra("DATA_TO_RETURN");
            showResult(returnedData);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showResult(@Nullable String text) {
        if (text != null) {
            viewResultText.setText(text);
            Log.d(TAG, "text: " + text);
        }
    }
}
