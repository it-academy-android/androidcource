package by.itacademy.cource.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class ResultActivity extends AppCompatActivity {

    private static final String TAG = "ResultActivity";

    private EditText viewEditResultText;
    private Button viewButtonDone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_result);

        viewEditResultText = findViewById(R.id.viewResultTextEdit);

        // Получаем аргументы переданные из MainActivity
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("EXTRA_DATA")){
            // Забираем данные из интента и сразу ставим их в хинт(подсказку) нашего текстового поля
            viewEditResultText.setHint(intent.getStringExtra("EXTRA_DATA"));
        }

        viewButtonDone = findViewById(R.id.viewButtonDone);
        viewButtonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Считываем результаты и возвращаем их в MainActivity
                String textToReturn = viewEditResultText.getText().toString();
                finishWithResult(textToReturn);
            }
        });

        if (savedInstanceState != null) {
            String savedText = savedInstanceState.getString("DATA_TO_SAVE");
            if (savedText != null) {
                Log.d(TAG, "onCreate savedInstanceState: " + savedText);
                viewEditResultText.setText(savedText);
            }
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            String savedText = savedInstanceState.getString("DATA_TO_SAVE");
            if (savedText != null) {
                Log.d(TAG, "onRestoreInstanceState savedInstanceState: " + savedText);
            }
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        if (!viewEditResultText.getText().toString().trim().isEmpty()) {
            Log.d(TAG, "onSaveInstanceState: " + viewEditResultText.getText().toString());
            outState.putString("DATA_TO_SAVE", viewEditResultText.getText().toString());
        }
        super.onSaveInstanceState(outState);
    }


    private void finishWithResult(String textToReturn) {
        // Считываем результаты и возвращаем их в MainActivity
        Intent dataToReturn = new Intent();
        if (textToReturn.trim().isEmpty()) {
            setResult(RESULT_CANCELED, dataToReturn);
        } else {
            dataToReturn.putExtra("DATA_TO_RETURN", textToReturn);
            setResult(RESULT_OK, dataToReturn);
        }
        finish();
    }
}
